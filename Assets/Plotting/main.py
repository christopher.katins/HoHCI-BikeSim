from typing import Optional

import dash
import dash_html_components as html
import dash_core_components as dcc
import os

from dash.dependencies import Input, Output
from functools import reduce

# Load data
from data import Data
from tools import get_options, generate_figure

logs_path = "../Logs/Subject_1909"
log_files = sorted([file for file in os.listdir(logs_path) if not '.' in file], reverse=True)
d1 = Data(logs_path)
d2 = Data(logs_path)

# df = pd.read_csv('data/stockdata2.csv', index_col=0, parse_dates=True)
# df.index = pd.to_datetime(df['Date'])

# Initialize the app
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

app.layout = html.Div(
    children=[
        html.Div(className='row',
                 children=[
                     html.Div(className='four columns div-user-controls',
                              children=[
                                  html.H2('VR Cycling - Visualize Logs'),
                                  html.Div(
                                      className='div-for-dropdown-group',
                                      children=[
                                          html.H3('First plot:'),
                                          dcc.Dropdown(id='select-file-1',
                                                       multi=False,
                                                       options=get_options(d1.read_folder()),
                                                       style={'backgroundColor': '#1E1E1E'}
                                                       ),
                                          html.H3('Pick one or more data items.'),
                                          dcc.Dropdown(id='select-data-1',
                                                       multi=True,
                                                       style={'backgroundColor': '#1E1E1E'}
                                                       )
                                      ]
                                  ),

                                  html.Div(
                                      className='div-for-dropdown-group',
                                      children=[
                                          html.H3('Second plot:'),
                                          dcc.Dropdown(id='select-file-2',
                                                       multi=False,
                                                       options=get_options(d1.read_folder()),
                                                       style={'backgroundColor': '#1E1E1E'}
                                                       ),
                                          html.H3('Pick one or more data items.'),
                                          dcc.Dropdown(id='select-data-2',
                                                       multi=True,
                                                       style={'backgroundColor': '#1E1E1E'}
                                                       )
                                      ])
                              ]
                              ),
                     html.Div(className='eight columns div-for-charts bg-grey',
                              children=[
                                  dcc.Graph(id='data-plot-1', config={'displayModeBar': False}, animate=True),
                                  html.Div(style={'height': '12px'}),
                                  dcc.Graph(id='data-plot-2', config={'displayModeBar': False}, animate=True),
                                  # dcc.Interval(
                                  #    id='interval-component',
                                  #    interval=1 * 1000,  # in milliseconds
                                  #    n_intervals=0
                                  # )
                              ])
                 ])
    ]

)


@app.callback(Output('select-data-1', 'options'),
              [Input('select-file-1', 'value')])
def update_file_selection_1(selected_file):
    d1.read_data(selected_file)
    return get_options(d1.get_columns())


@app.callback(Output('select-data-2', 'options'),
              [Input('select-file-2', 'value')])
def update_file_selection_2(selected_file):
    d2.read_data(selected_file)
    return get_options(d2.get_columns())


@app.callback(Output('data-plot-1', 'figure'),
              [Input('select-data-1', 'value')])
def update_plot_1(selected_data_columns):
    # print(f'Interval {n}')
    return generate_figure(selected_data_columns, d1.df,
                           f'Log Data - {d1.current_file_name}')


@app.callback(Output('data-plot-2', 'figure'), [Input('select-data-2', 'value')])
def update_plot_1(selected_data_columns):
    return generate_figure(selected_data_columns, d2.df,
                           f'Log Data - {d2.current_file_name}')


if __name__ == '__main__':
    app.run_server(debug=True)
