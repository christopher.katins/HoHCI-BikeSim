import plotly.graph_objects as go
import math
import pandas as pd

def get_options(options_list):
    dict_list = []
    for i in options_list:
        dict_list.append({'label': i, 'value': i})

    return dict_list

def generate_figure(cols, df, title):
    trace = []
    # STEP 2
    # Draw and append traces for each stock
    for c in cols if cols is not None else []:
        s = df[c].dropna()
        trace.append(go.Scatter(x=s.index,
                                y=s,
                                mode='lines',
                                opacity=0.7,
                                name=c,
                                textposition='bottom center',
                                showlegend=True))
        # STEP 3
    traces = [trace]
    data = [val for sublist in traces for val in sublist]
    # Define Figure
    # STEP 4
    figure = {'data': data,
              'layout': go.Layout(
                  colorway=["#5E0DAC", '#FF4F00', '#375CB1', '#FF7400', '#FFF400', '#FF0056'],
                  template='plotly_dark',
                  paper_bgcolor='rgba(0, 0, 0, 0)',
                  plot_bgcolor='rgba(0, 0, 0, 0)',
                  margin={'b': 15},
                  hovermode='x',
                  autosize=True,
                  title={'text': title,
                         'font': {'color': 'white'}, 'x': 0.5},
                  xaxis={'range': [df.index.min() if df is not None else 0,
                                   df.index.max() if df is not None else 0]},
              ),

              }

    return figure