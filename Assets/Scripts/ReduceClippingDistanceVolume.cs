﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ReduceClippingDistanceVolume : MonoBehaviour
{
    public int reducePreClippingDistanceTo = 300;
    public int reduceSmallObjectsDistanceTo = 100;

    private IgnoreClipping ignoreClipping;

    private bool reduced = false;

    private void Start()
    {
        var cam = Camera.main;
        if (cam == null)
        {
            Debug.LogError("No camera found!");
            return;
        }

        ignoreClipping = cam.GetComponent<IgnoreClipping>();
        if (ignoreClipping == null)
        {
            Debug.LogError("No IgnoreClipping attached to Main Camera");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("bike") || reduced) return;
        ignoreClipping.applyDistances(reducePreClippingDistanceTo, reduceSmallObjectsDistanceTo);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("bike") || !reduced) return;
        ignoreClipping.applyDistances();
    }
}