﻿using System.Text;
using Controller.Bicycle;
using Sensors;
using UnityEngine;

namespace Display
{
    public class DebugDisplay<T> : MonoBehaviour where T : IBicycleController
    {
        public T bicycleController;
        private readonly BikeSensorData sensorData = BikeSensorData.Instance;

        private string ip;
        private string text;


        private void Start()
        {
            ip = Helpers.GetIPAddress().ToString();
        }

        private void Update()
        {
            var rbSpeed = bicycleController.RigidBodyVelocity.magnitude * 3.6;
            var sensorSpeed = sensorData.SpeedData?.SpeedKmh ?? 0f;
            var sb = new StringBuilder();
            //sb.Append(
            //    $"MotorTorque: {bicycleController.CurrentMotorTorque:n2}\nBrakeTorque: {bicycleController.CurrentBrakeTorque:n2}\nSteer: {bicycleController.CurrentSteerAngle}\nLean: {bicycleController.CurrentLeaningAngle:n4}\n");
            sb.Append("-----Sensors------\n");
            sb.Append(
                $"Speed: {sensorData.SpeedData?.Speed ?? 0f:n4} m/s\nPolar Acc ({sensorData.BleData?.Acc.x ?? 0f:n4}; {sensorData.BleData?.Acc.y ?? 0f:n4}; {sensorData.BleData?.Acc.z ?? 0f:n4}) ");
            sb.Append("\n\n");
            sb.Append($"IP: {ip}\n");
            sb.Append("----BicycleController-----\n");
            sb.Append($"Actual Speed {rbSpeed:n2} km/h - Dif: {bicycleController.CurrentSpeed * 3.6 - rbSpeed:n2}\n");
            sb.Append(
                $"Current Speed = {bicycleController.CurrentSpeed:n2} m/s ({bicycleController.CurrentSpeed * 3.6:n2} km/h)");
            text = sb.ToString();
        }

        private void OnGUI()
        {
            var padding = 10;
            var width = 260;
            var height = 175;
            var x = Screen.width - width - padding;
            var y = Screen.height - height - padding;

            GUI.TextField(new Rect(x, y, width, height), text);
        }
    }
}