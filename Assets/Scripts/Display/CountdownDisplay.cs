﻿using TMPro;
using UnityEngine;

namespace Display
{
    public class CountdownDisplay : InFrontOfCameraDisplay
    {
        public delegate string CountdownDoneCallback();

        public int countdown = 3;
        public TextMeshProUGUI countdownText;

        private float lastTime;

        public CountdownDoneCallback OnCountdownDone { get; set; }

        protected override void Start()
        {
            base.Start();
            lastTime = Time.time;
        }

        protected override void Update()
        {
            var t = Time.time;
            if (t - lastTime >= 1)
            {
                lastTime = t;
                countdown--;
                countdownText.text = $"{countdown}";
                if (countdown == 0)
                {
                    countdownText.text = OnCountdownDone?.Invoke();
                    Destroy(gameObject, 0.5f);
                }
            }

            base.Update();
        }
    }
}