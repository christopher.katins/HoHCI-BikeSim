﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

namespace Display
{
    public class LookStraightDisplay : MonoBehaviour
    {
        public delegate void DoneCallback();

        public Image circleImage;
        public float timeToLookStraight = 1.5f;
        private Transform camTransform;
        private readonly Color endColor = Color.green.ColorWithAlpha(100);

        private List<Vector3> positions;
        private List<Quaternion> quaternions;

        private readonly Color startColor = Color.white.ColorWithAlpha(100);

        private float startTime;

        public DoneCallback OnDone { get; set; }


        private void Update()
        {
            var timePassed = Time.time - startTime;
            var currentColor = Color.Lerp(startColor, endColor, timePassed / timeToLookStraight);
            circleImage.color = currentColor;

            if (timePassed >= timeToLookStraight)
            {
                OnDone();
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }

        private void FixedUpdate()
        {
            //Debug.Log($"GameObject.Layer = {gameObject.layer}");
            var layerMask = 1 << gameObject.layer;
            if (Physics.Raycast(camTransform.position, camTransform.forward, out var hit, 2f, layerMask))
            {
                Debug.DrawRay(camTransform.position, camTransform.forward * hit.distance, Color.green);
                //Debug.Log($"Hit something ({hit.collider.gameObject.layer})");
            }
            else
            {
                Debug.DrawRay(camTransform.position, camTransform.forward * 2f, Color.red);
                //Debug.Log("Hit nothing");
                ResetCircle();
            }
        }

        private void OnEnable()
        {
            circleImage = GetComponentInChildren<Image>();
            ResetCircle();

            var cam = Camera.main;
            if (cam == null) throw new Exception("No camera available");

            camTransform = cam.transform;
        }

        private void ResetCircle()
        {
            circleImage.color = startColor;
            startTime = Time.time;
        }
    }
}