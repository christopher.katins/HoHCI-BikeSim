﻿using Controller.Bicycle;
using Sensors;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Display
{
    public class BikeDataDisplay : MonoBehaviour
    {
        [FormerlySerializedAs("usePolarForHeartRate")]
        public bool useBleForHeartRate = true;

        public bool useBleForPower = true;

        public TextMeshProUGUI cadenceDisplay;
        public TextMeshProUGUI heartRateDisplay;

        public TextMeshProUGUI powerDisplay;

        [Header("Speed")] public RbBicycleController bicycleController;
        public TextMeshProUGUI speedDisplay;
        public bool useSpeedFromBicycleController = true;
        public TextMeshProUGUI torqueDisplay;
        private bool cadenceDisplayAvailable;
        private bool heartRateDisplayAvailable;
        private bool powerDisplayAvailable;

        private BikeSensorData sensorData;


        private bool speedDisplayAvailable;
        private bool torqueDisplayAvailable;

        // Start is called before the first frame update
        private void Start()
        {
            sensorData = BikeSensorData.Instance;
            speedDisplayAvailable = speedDisplay != null;
            powerDisplayAvailable = powerDisplay != null;
            torqueDisplayAvailable = torqueDisplay != null;
            cadenceDisplayAvailable = cadenceDisplay != null;
            heartRateDisplayAvailable = heartRateDisplay != null;
        }

        // Update is called once per frame
        private void Update()
        {
            var bikeSpeed = useSpeedFromBicycleController
                ? bicycleController.CurrentSpeedKph
                : sensorData.SpeedData?.SpeedKmh ?? 0f;

            if (speedDisplayAvailable) speedDisplay.text = $"{bikeSpeed:n2} km/h";

            var powerData = useBleForPower
                ? sensorData.BleData?.PowermeterData.power ?? 0f
                : sensorData.PowermeterData?.InstantaneousPower ?? 0f;

            var cadenceData = useBleForPower
                ? sensorData.BleData?.PowermeterData.cadence ?? 0f
                : sensorData.PowermeterData?.InstantaneousCadence ?? 0f;

            var torqueData = useBleForPower
                ? sensorData.BleData?.PowermeterData.torque ?? 0f
                : sensorData.PowermeterData?.CrankTorque ?? 0f;

            if (powerDisplayAvailable)
                powerDisplay.text =
                    $"{powerData:0} W";

            if (cadenceDisplayAvailable)
                cadenceDisplay.text = $"{cadenceData:0} rpm";

            if (torqueDisplayAvailable) torqueDisplay.text = $"{torqueData:F2} Nm";

            if (heartRateDisplayAvailable)
                heartRateDisplay.text =
                    $"{(useBleForHeartRate ? sensorData.BleData?.Hr : sensorData.HrData?.HeartRate) ?? 0:0} bpm";
        }
    }
}