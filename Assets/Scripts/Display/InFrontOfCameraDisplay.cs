﻿using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Display
{
    public class InFrontOfCameraDisplay : MonoBehaviour
    {
        public float distance;
        private Camera cam;
        private Transform camTransform;
        private Transform canvasTransform;

        protected virtual void Start()
        {
            cam = Camera.main;
            Debug.Assert(cam != null, nameof(cam) + " != null");
            camTransform = cam.transform;
            canvasTransform = GetComponentInChildren<Canvas>().transform;
        }

        protected virtual void Update()
        {
            canvasTransform.SetPositionAndRotation(camTransform.position + camTransform.forward * distance,
                camTransform.rotation);
        }
    }
}