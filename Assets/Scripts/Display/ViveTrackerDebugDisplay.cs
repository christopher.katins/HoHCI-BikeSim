﻿using TMPro;
using Tracking;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace Display
{
    public class ViveTrackerDebugDisplay : MonoBehaviour
    {
        public TextMeshProUGUI posText;
        public TextMeshProUGUI rotText;
        public TextMeshProUGUI batteryTextFw;
        public TextMeshProUGUI batteryTextLeg;
        public TextMeshProUGUI statusTextFw;
        public TextMeshProUGUI statusTextLeg;

        public Image fwImage;
        public Image legImage;

        public FrontWheelTracker fwTracker;
        public KineticLegTracker legTracker;

        private SteamVR_TrackedObject fwTrackedObject;
        private bool isbatteryTextFwNotNull;
        private bool isbatteryTextLegNotNull;

        private bool isFwTrackedObjectNull;

        private bool isFwTrackerNull;
        private bool isLegTrackedObjectNull;
        private bool islegTrackerNull;
        private bool isposTextNotNull;
        private bool isrotTextNotNull;
        private bool isstatusTextFwNotNull;
        private bool isstatusTextLegNotNull;
        private SteamVR_TrackedObject legTrackedObject;

        // Start is called before the first frame update
        private void Start()
        {
            isstatusTextLegNotNull = statusTextLeg != null;
            isstatusTextFwNotNull = statusTextFw != null;
            isbatteryTextLegNotNull = batteryTextLeg != null;
            isbatteryTextFwNotNull = batteryTextFw != null;
            isposTextNotNull = posText != null;
            isrotTextNotNull = rotText != null;
            isFwTrackerNull = fwTracker == null;
            islegTrackerNull = legTracker == null;

            fwTrackedObject = fwTracker.GetComponent<SteamVR_TrackedObject>();
            legTrackedObject = legTracker.GetComponent<SteamVR_TrackedObject>();

            isFwTrackedObjectNull = fwTrackedObject == null;
            isLegTrackedObjectNull = legTrackedObject == null;
        }

        // Update is called once per frame
        private void Update()
        {
            if (!isFwTrackerNull & fwTracker.isActiveAndEnabled & !isFwTrackerNull && fwTrackedObject.isValid)
            {
                if (isposTextNotNull) posText.text = $"Pos: {fwTracker.RelativePosition.ToString()}";
                if (isrotTextNotNull) rotText.text = $"Rot: {fwTracker.SteerRotation:n2}";
                if (isbatteryTextFwNotNull) batteryTextFw.text = $"Battery: {fwTracker.BatteryLevel}";
                if (isstatusTextFwNotNull) statusTextFw.text = "Status: Connected";
                fwImage.color = Color.green;
            }
            else
            {
                if (isposTextNotNull) posText.text = "Pos: -";
                if (isrotTextNotNull) rotText.text = "Rot: -";
                fwImage.color = Color.grey.ColorWithAlpha(100);
                if (isstatusTextFwNotNull) statusTextFw.text = "Status: Disconnected";
            }

            if (!islegTrackerNull & legTracker.isActiveAndEnabled && !isLegTrackedObjectNull &&
                legTrackedObject.isValid)
            {
                legImage.color = Color.green;
                if (isbatteryTextLegNotNull) batteryTextLeg.text = $"Battery: {legTracker.BatteryLevel}";
                if (isstatusTextLegNotNull) statusTextLeg.text = "Status: Connected";
            }
            else
            {
                legImage.color = Color.grey.ColorWithAlpha(100);
                if (isstatusTextLegNotNull) statusTextLeg.text = "Status: Disconnected";
            }
        }
    }
}