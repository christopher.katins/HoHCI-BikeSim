﻿using System;
using System.Collections.Generic;
using Logging.Base;
using Logging.Data;
using UnityEngine;

namespace Logging
{
    public readonly struct FPSData : ISerializable
    {
        private readonly long timestamp;
        private readonly int fps;

        public FPSData(long timestamp, int fps)
        {
            this.timestamp = timestamp;
            this.fps = fps;
        }

        public KeyValuePair<long, string[]> Serialize()
        {
            return new KeyValuePair<long, string[]>(timestamp, new[]
            {
                fps.ToString()
            });
        }
    }

    public class FPSLogger : SensorDataLogger<FPSData>
    {
        private const float FPSMeasurePeriod = 0.5f;
        private int currentFps;
        private int fpsAccumulator;
        private float fpsNextPeriod;
        public override string Key => "fps";


        public override void Start()
        {
            base.Start();
            fpsNextPeriod = Time.realtimeSinceStartup + FPSMeasurePeriod;
        }

        private void Update()
        {
            fpsAccumulator++;
            if (Time.realtimeSinceStartup > fpsNextPeriod)
            {
                currentFps = (int) (fpsAccumulator / FPSMeasurePeriod);
                fpsAccumulator = 0;
                fpsNextPeriod += FPSMeasurePeriod;
            }

            //Debug.Log($"Current FPS = {currentFps}");

            Log(new FPSData(Helpers.RoundToLong(Time.time * 1000f), currentFps));
        }


        public override IEnumerable<FPSData> ReadLog(IEnumerable<IEnumerable<string>> lines)
        {
            throw new NotImplementedException();
        }
    }
}