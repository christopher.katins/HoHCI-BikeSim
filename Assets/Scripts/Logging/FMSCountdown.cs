﻿using Routes;
using UnityEngine;

namespace Logging
{
    public class FMSCountdown : MonoBehaviour
    {
        public RouteManager routeManager;
        public int fmsInterval = 30;

        private float startTime = -1f;
        private float timestampLastFMS;

        private GUIStyle bigRedTextStyle;

        private void Start()
        {
            bigRedTextStyle = new GUIStyle {fontSize = 72, normal = new GUIStyleState {textColor = Color.red}};
            var activeRoute = routeManager.routes[routeManager.selectedRoute];
            activeRoute.OnStartEntered += () =>
            {
                startTime = Time.time;
                timestampLastFMS = startTime;
            };
            activeRoute.OnFinishPassed += () => gameObject.SetActive(false);
        }

        private void OnGUI()
        {
            if (startTime < 0) return;
            var time = Time.time - startTime;
            var timeFromLastFms = Time.time - timestampLastFMS;
            GUI.TextField(new Rect(20, 85, 200, 20), $"Seconds Passed: {time:N2}");

            var distToInterval = fmsInterval - timeFromLastFms;
            if (distToInterval <= 1 || distToInterval >= fmsInterval - 2 && time > fmsInterval) //show a second before and 2 after
            {
                GUI.TextField(new Rect(225, 65, 120, 100), "FMS!", bigRedTextStyle);
                if (distToInterval <= 0) // set timestamp for next interval
                {
                    timestampLastFMS = Time.time;
                }
            }
        }
    }
}