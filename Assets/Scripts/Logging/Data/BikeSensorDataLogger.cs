﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Logging.Base;
using Sensors;
using UnityEngine;

namespace Logging.Data
{
    public readonly struct BikeSensorDataLog : ISerializable
    {
        private readonly long timestamp;
        private readonly float speed;
        private readonly float cadence;
        private readonly int heartRate;
        private readonly float torque;
        private readonly float power;

        public BikeSensorDataLog(long timestamp, float speed, float cadence, int heartRate, float torque, float power)
        {
            this.timestamp = timestamp;
            this.speed = speed;
            this.cadence = cadence;
            this.heartRate = heartRate;
            this.torque = torque;
            this.power = power;
        }

        public KeyValuePair<long, string[]> Serialize()
        {
            return new KeyValuePair<long, string[]>(timestamp, new[]
            {
                speed.ToString("F4", CultureInfo.InvariantCulture),
                cadence.ToString("F4", CultureInfo.InvariantCulture),
                heartRate.ToString(),
                torque.ToString("F4", CultureInfo.InvariantCulture),
                power.ToString("F4", CultureInfo.InvariantCulture)
            });
        }
    }

    public class BikeSensorDataLogger : SensorDataLogger<BikeSensorDataLog>
    {
        [Tooltip("Uses Heart Rate transmitted by bluetooth instead of ANT+")]
        public bool usePolarHeartRate = true;

        public bool useBlePowerData = true;

        private BikeSensorData bikeSensorData;
        public override string Key => "bike_sensor_data";

        public override void Start()
        {
            base.Start();
            bikeSensorData = BikeSensorData.Instance;
        }

        private void Update()
        {
            var hr = usePolarHeartRate
                ? bikeSensorData.BleData?.Hr ?? -1
                : (int) (bikeSensorData.HrData?.HeartRate ?? -1);
            var speed = bikeSensorData.SpeedData?.SpeedKmh ?? -1f;
            var power = useBlePowerData
                ? bikeSensorData.BleData?.PowermeterData.power ?? -1f
                : bikeSensorData.PowermeterData?.InstantaneousPower ?? -1f;
            var cadence = useBlePowerData
                ? bikeSensorData.BleData?.PowermeterData.cadence ?? -1f
                : bikeSensorData.PowermeterData?.InstantaneousCadence ?? -1f;
            var torque = useBlePowerData
                ? bikeSensorData.BleData?.PowermeterData.torque ?? -1f
                : bikeSensorData.PowermeterData?.CrankTorque ?? -1f;
            Log(new BikeSensorDataLog(Helpers.RoundToLong(Time.time * 1000), speed, cadence, hr, torque, power));
        }


        public override IEnumerable<BikeSensorDataLog> ReadLog(IEnumerable<IEnumerable<string>> lines)
        {
            throw new NotImplementedException(); //TODO
        }
    }
}