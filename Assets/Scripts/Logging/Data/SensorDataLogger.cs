﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Logging.Base;
using UnityEngine;

namespace Logging.Data
{
    public abstract class SensorDataLogger<T> : MonoBehaviour, ILogable, ISerializableLog<T> where T : ISerializable
    {
        public virtual void Awake()
        {
            gameObject.SetActive(GameManager.LOG_TO_FILE);
        }

        public virtual void Start()
        {
            BufferLines = new Dictionary<long, string[]>();
            FileLogger.Instance.RegisterLogable(this);
        }

        public abstract string Key { get; }

        public IEnumerable<string> HeaderNames =>
            typeof(T).GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Select(f => f.Name)
                .Where(f => !f.Equals("timestamp"));

        public Dictionary<long, string[]> BufferLines { get; private set; }

        public void ClearBuffer()
        {
            BufferLines.Clear();
        }

        public void Log(T value)
        {
            var data = value.Serialize();
            BufferLines[data.Key] = data.Value;
        }

        public abstract IEnumerable<T> ReadLog(IEnumerable<IEnumerable<string>> lines);
    }
}