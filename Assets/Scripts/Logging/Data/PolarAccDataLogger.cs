﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Logging.Base;
using Sensors;
using Sensors.Bluetooth;
using Sensors.Polar;
using UniRx;

namespace Logging.Data
{
    public readonly struct PolarAccDataLog : ISerializable
    {
        private readonly long timestamp;
        private readonly float accX;
        private readonly float accY;
        private readonly float accZ;

        public PolarAccDataLog(long timestamp, float accX, float accY, float accZ)
        {
            this.timestamp = timestamp;
            this.accX = accX;
            this.accY = accY;
            this.accZ = accZ;
        }

        public KeyValuePair<long, string[]> Serialize()
        {
            return new KeyValuePair<long, string[]>(timestamp, new[]
            {
                accX.ToString("F4", CultureInfo.InvariantCulture),
                accY.ToString("F4", CultureInfo.InvariantCulture),
                accZ.ToString("F4", CultureInfo.InvariantCulture)
            });
        }
    }

    public class PolarAccDataLogger : SensorDataLogger<PolarAccDataLog>
    {
        private long startTimeAcc = -1;

        private IDisposable sub;
        private TimeSync timeSync;
        public override string Key => "polar_acc_data";

        public override async void Start()
        {
            base.Start();
            timeSync = new TimeSync
            {
                StartTime = DateTime.Now,
                DifDataStreamStart = -1
            };
            var bikeSensorData = BikeSensorData.Instance;
            await bikeSensorData.PolarReceiverAvailable;
            sub = bikeSensorData.RawAccData?.Subscribe(data => OnData(data));
        }

        private void OnDestroy()
        {
            sub?.Dispose();
        }

        private void OnData(in AccData data)
        {
            if (timeSync.DifDataStreamStart < 0)
                timeSync.DifDataStreamStart = (long) (DateTime.Now - timeSync.StartTime).TotalMilliseconds;

            if (startTimeAcc < 0) startTimeAcc = data.Timestamp;

            if (BikeSensorData.Instance.PolarConfig == null) return;

            var internalTimestamp =
                (data.Timestamp - startTimeAcc) / 1000000;
            var timestamp = Helpers.RoundToLong(internalTimestamp + timeSync.DifDataStreamStart);
            foreach (var item in data.Values)
            {
                Log(new PolarAccDataLog(timestamp, item.x, item.y, item.z));
                timestamp += 1000 / BikeSensorData.Instance.PolarConfig.Value.accSampleRate;
            }
        }

        public override IEnumerable<PolarAccDataLog> ReadLog(IEnumerable<IEnumerable<string>> lines)
        {
            throw new NotImplementedException(); //TODO
        }
    }
}