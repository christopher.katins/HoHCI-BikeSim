﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Logging.Base;
using UnityEngine;

namespace Logging.Data
{
    public readonly struct BikeGameObjectDataLog : ISerializable
    {
        private readonly long timestamp;
        private readonly float positionX;
        private readonly float positionY;
        private readonly float positionZ;
        private readonly float rotationX;
        private readonly float rotationY;
        private readonly float rotationZ;
        private readonly float velocityX;
        private readonly float velocityY;
        private readonly float velocityZ;

        public BikeGameObjectDataLog(long timestamp, float positionX, float positionY, float positionZ, float rotationX,
            float rotationY, float rotationZ, float velocityX, float velocityY, float velocityZ)
        {
            this.timestamp = timestamp;
            this.positionX = positionX;
            this.positionY = positionY;
            this.positionZ = positionZ;
            this.rotationX = rotationX;
            this.rotationY = rotationY;
            this.rotationZ = rotationZ;
            this.velocityX = velocityX;
            this.velocityY = velocityY;
            this.velocityZ = velocityZ;
        }

        public KeyValuePair<long, string[]> Serialize()
        {
            return new KeyValuePair<long, string[]>(
                timestamp, new[]
                {
                    positionX.ToString("F6", CultureInfo.InvariantCulture),
                    positionY.ToString("F6", CultureInfo.InvariantCulture),
                    positionZ.ToString("F6", CultureInfo.InvariantCulture),
                    rotationX.ToString("F6", CultureInfo.InvariantCulture),
                    rotationY.ToString("F6", CultureInfo.InvariantCulture),
                    rotationZ.ToString("F6", CultureInfo.InvariantCulture),
                    velocityX.ToString("F6", CultureInfo.InvariantCulture),
                    velocityY.ToString("F6", CultureInfo.InvariantCulture),
                    velocityZ.ToString("F6", CultureInfo.InvariantCulture)
                });
        }
    }

    public class BikeGameObjectDataLogger : SensorDataLogger<BikeGameObjectDataLog>
    {
        public Rigidbody bike;
        private Transform bikeTransform;
        public override string Key => "bike_game_object_data";

        public override void Start()
        {
            base.Start();
            bikeTransform = bike.transform;
        }

        private void Update()
        {
            var pos = bikeTransform.position;
            var rot = bikeTransform.rotation.eulerAngles;
            var velocity = bike.velocity;

            Log(new BikeGameObjectDataLog(Helpers.RoundToLong(Time.time * 1000),
                pos.x, pos.y, pos.z,
                rot.x, rot.y, rot.z,
                velocity.x, velocity.y, velocity.z));
        }

        public override IEnumerable<BikeGameObjectDataLog> ReadLog(IEnumerable<IEnumerable<string>> lines)
        {
            throw new NotImplementedException(); //TODO
        }
    }
}