﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Logging.Base
{
    public class AsyncLogFileWriter
    {
        private readonly StreamWriter writer;

        public AsyncLogFileWriter(string path)
        {
            var fileInfo = new FileInfo(path);
            if (!fileInfo.Directory?.Exists ?? false) fileInfo.Directory?.Create();
            writer = new StreamWriter(path) {AutoFlush = true};
        }

        [Obsolete("Use WriteDataLine(values)")] //TODO
        public async Task WriteDataLine<T>(T xValue, IEnumerable<T> values)
        {
            await writer.WriteLineAsync($"{xValue}\t{string.Join("\t", values)}");
        }

        public async Task WriteDataLine<T>(IEnumerable<T> values)
        {
            await writer.WriteLineAsync($"{string.Join("\t", values)}");
        }

        public async Task WriteDataLines<T>(IEnumerable<IEnumerable<T>> lines)
        {
            //This method is ultimately called by an Update Method. Unity doesn't await Update

            var sb = new StringBuilder();
            foreach (var l in lines) sb.Append($"{string.Join("\t", l)}\n");

            await writer.WriteAsync(sb.ToString());
        }

        public void Dispose()
        {
            writer?.Close();
        }
    }
}