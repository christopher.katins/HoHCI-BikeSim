﻿using System.Collections.Generic;

namespace Logging.Base
{
    public interface ISerializable
    {
        KeyValuePair<long, string[]> Serialize();
    }
}