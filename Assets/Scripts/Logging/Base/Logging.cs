﻿using System.Threading.Tasks;
using Study;
using UnityEngine;

namespace Logging.Base
{
    public class Logging : MonoBehaviour
    {
        public float writeInterval = 1f;
        public SubjectManager subjectManager;
        public ConditionManager conditionManager;
        private FileLogger logger;
        private Task previousLogUpdateTask = Task.CompletedTask;

        private float prevLogTimestamp;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        private void Start()
        {
            logger = FileLogger.Instance;
            logger.Condition = $"Condition_{conditionManager.activeCondition}";
            logger.SubFolderName = $"Subject_{subjectManager.subjectIdentifier}";
        }

        private void Update()
        {
            if (!previousLogUpdateTask.IsCompleted) return;
            var time = Time.time;
            var dif = time - prevLogTimestamp;
            if (dif >= writeInterval)
            {
                prevLogTimestamp = time;
                previousLogUpdateTask = logger.UpdateRegisteredLogs();
            }
        }

        private async void OnDestroy()
        {
            if (logger != null) await logger.UpdateRegisteredLogs();
            FileLogger.DestroyInstance();
        }
    }
}