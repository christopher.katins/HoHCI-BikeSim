﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Valve.VR.InteractionSystem;

namespace Logging.Base
{
    public class FileLogger
    {
        private readonly List<ILogable> logables;

        private readonly DateTime startTimestamp;
        private readonly Dictionary<string, Dictionary<long, string[]>> waitingBuffers;
        private AsyncLogFileWriter writer;
        private bool writerAvailable;

        private FileLogger()
        {
            waitingBuffers = new Dictionary<string, Dictionary<long, string[]>>();
            startTimestamp = DateTime.Now;
            logables = new List<ILogable>();
        }

        public string SubFolderName { get; set; }
        public string Condition { get; set; }

        public void RegisterLogable(ILogable l)
        {
            logables.Add(l);
        }


        private IEnumerable<string> MergedHeaders()
        {
            var headers = new List<string> {"timestamp"};
            foreach (var l in logables) headers.AddRange(l.HeaderNames);

            return headers.ToArray();
        }

        private IEnumerable<long> MergeTimestamps()
        {
            var allTimestamps = new HashSet<long>();

            long minBegin = -1;
            long minEnd = -1;
            foreach (var l in logables)
            {
                var keys = l.BufferLines.Keys.ToArray();
                var begin = keys.Length > 0 ? keys.First() : -1;
                var end = keys.Length > 0 ? keys.Last() : -1;

                if (minBegin == -1 || begin >= 0 && begin < minBegin) minBegin = begin;
                if (minEnd == -1 || end >= 0 && end < minEnd) minEnd = end;
            }

            foreach (var l in logables)
            {
                var bufferLines = l.BufferLines;
                var timestamps = new HashSet<long>(bufferLines.Keys);
                var logableKey = l.Key;
                var hasWaiting = waitingBuffers.TryGetValue(logableKey, out var waiting);

                if (hasWaiting)
                    foreach (var waitingTs in waiting.Keys)
                        if (waitingTs <= minEnd)
                            allTimestamps.Add(waitingTs);

                foreach (var t in timestamps)
                    if (t >= minBegin && t <= minEnd)
                    {
                        allTimestamps.Add(t);
                    }
                    else
                    {
                        if (waitingBuffers.ContainsKey(l.Key))
                            waitingBuffers[l.Key][t] = bufferLines[t];
                        else
                            waitingBuffers[l.Key] = new Dictionary<long, string[]> {{t, bufferLines[t]}};
                    }
            }

            logables.ForEach(l => allTimestamps.UnionWith(l.BufferLines.Keys.Where(k => k >= minBegin && k <= minEnd)));

            return allTimestamps;
        }

        private IEnumerable<IEnumerable<string>> MergedLines()
        {
            var lines = new List<List<string>>();
            var allKeys = MergeTimestamps();

            var mergedDict = new SortedDictionary<long, List<string>>();
            foreach (var timestamp in allKeys)
            {
                var newItem = new List<string>();
                foreach (var l in logables)
                {
                    var logableKey = l.Key;
                    var hasWaiting = waitingBuffers.TryGetValue(logableKey, out var waiting);
                    string[] linePart;

                    if (l.BufferLines.ContainsKey(timestamp))
                    {
                        linePart = l.BufferLines[timestamp];
                    }
                    else if (hasWaiting && waiting.ContainsKey(timestamp))
                    {
                        linePart = waiting[timestamp];
                        waiting.Remove(timestamp);
                    }
                    else
                    {
                        /*var lastAvailableKey =
                            l.BufferLines.Keys.Where(k => k < timestamp).DefaultIfEmpty(-1).LastOrDefault();
                        linePart = lastAvailableKey >= 0
                            ? l.BufferLines[lastAvailableKey]
                            : new string[l.HeaderNames.ToArray().Length];*/
                        linePart = new string[l.HeaderNames.ToArray().Length];
                    }

                    newItem.AddRange(linePart);
                }

                mergedDict[timestamp] = newItem;
            }

            mergedDict.ForEach(item =>
            {
                var listItem = new List<string> {item.Key.ToString("D")};
                listItem.AddRange(item.Value);
                lines.Add(listItem);
            });
            return lines;
        }

        public async Task UpdateRegisteredLogs()
        {
            if (!writerAvailable)
            {
                var subfolder = SubFolderName != null ? $"/{SubFolderName}/" : "";
                var condition = $"_{Condition}" ?? "";
                writer = new AsyncLogFileWriter(
                    $"Assets/Logs/{subfolder}{startTimestamp:yyyy-MM-dd_HHmmss}{condition}.tsv");
                await writer.WriteDataLine(MergedHeaders());
                writerAvailable = true;
            }

            await writer.WriteDataLines(MergedLines());
            logables.ForEach(l => l.ClearBuffer());
        }

        private void Dispose()
        {
            if (writerAvailable) writer.Dispose();

            logables.Clear();
        }

        #region signleton

        private static Lazy<FileLogger>
            instance =
                new Lazy<FileLogger>
                    (() => new FileLogger());

        public static FileLogger Instance => instance.Value;

        public static void DestroyInstance()
        {
            if (instance.IsValueCreated)
            {
                instance.Value.Dispose();
                instance =
                    new Lazy<FileLogger>
                        (() => new FileLogger());
            }
        }

        #endregion
    }
}