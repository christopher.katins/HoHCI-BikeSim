﻿using System;

namespace Logging.Base
{
    public struct TimeSync
    {
        public DateTime StartTime;
        public long DifDataStreamStart;
    }
}