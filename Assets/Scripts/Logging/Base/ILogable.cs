﻿using System.Collections.Generic;

namespace Logging.Base
{
    public interface ILogable
    {
        string Key { get; }
        IEnumerable<string> HeaderNames { get; } // header names apart from timestamp

        Dictionary<long, string[]> BufferLines { get; } //key = timestamp, rest is serialized values

        void ClearBuffer();
    }

    public interface ISerializableLog<T>
    {
        void Log(T value);

        IEnumerable<T> ReadLog(IEnumerable<IEnumerable<string>> lines);
    }
}