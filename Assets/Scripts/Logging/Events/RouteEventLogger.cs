﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Logging.Base;
using Logging.Data;
using Roads;
using Routes;
using UnityEngine;

namespace Logging.Events
{
    public enum RouteEvent
    {
        None,
        Left,
        Right,
        Straight,
        Start,
        Finish
    }

    public readonly struct RouteEventData : ISerializable
    {
        private readonly long timestamp;
        private readonly RouteEvent eventRoute;

        public RouteEventData(long timestamp, RouteEvent eventRoute)
        {
            this.timestamp = timestamp;
            this.eventRoute = eventRoute;
        }

        public KeyValuePair<long, string[]> Serialize()
        {
            return new KeyValuePair<long, string[]>(timestamp, new[]
            {
                eventRoute.ToString()
            });
        }
    }

    public class RouteEventLogger : SensorDataLogger<RouteEventData>
    {
        public GameObject routes;

        [CanBeNull] private Route activeRoute;
        private bool activeRouteAvailable;
        private StraightRoadExtras finish;

        private StraightRoadExtras start;
        private IEnumerable<Turn> turns;
        public override string Key => "route_events";

        public override void Start()
        {
            base.Start();
            activeRoute = routes.GetComponentsInChildren<Route>().FirstOrDefault(r => r.isActiveAndEnabled);
            if (activeRoute == null) return;

            activeRouteAvailable = true;
            start = activeRoute.start;
            finish = activeRoute.finish;
            turns = activeRoute.items.Select(item => item.turn);
        }

        private void Update()
        {
            var e = RouteEvent.None;
            if (start.ArcState == TriggerState.Inside)
                e = RouteEvent.Start;
            else if (finish.ArcState == TriggerState.Inside)
                e = RouteEvent.Finish;
            else if (activeRouteAvailable) e = EventForActiveRoute();

            Log(new RouteEventData(Helpers.RoundToLong(Time.time * 1000f), e));
        }

        private RouteEvent EventForActiveRoute()
        {
            var currentTurn = turns.FirstOrDefault(t => t.BikeTriggerState == TriggerState.Inside);
            if (currentTurn == null) return RouteEvent.None;

            switch (currentTurn.comingFrom)
            {
                case RoadDirection.West:
                    switch (currentTurn.goingTo)
                    {
                        case RoadDirection.North:
                            return RouteEvent.Left;
                        case RoadDirection.East:
                            return RouteEvent.Straight;
                        case RoadDirection.South:
                            return RouteEvent.Right;
                        default:
                            return RouteEvent.None;
                    }
                case RoadDirection.North:
                    switch (currentTurn.goingTo)
                    {
                        case RoadDirection.East:
                            return RouteEvent.Left;
                        case RoadDirection.South:
                            return RouteEvent.Straight;
                        case RoadDirection.West:
                            return RouteEvent.Right;
                        default:
                            return RouteEvent.None;
                    }
                case RoadDirection.East:
                    switch (currentTurn.goingTo)
                    {
                        case RoadDirection.North:
                            return RouteEvent.Right;
                        case RoadDirection.South:
                            return RouteEvent.Left;
                        case RoadDirection.West:
                            return RouteEvent.Straight;
                        default:
                            return RouteEvent.None;
                    }
                case RoadDirection.South:
                    switch (currentTurn.goingTo)
                    {
                        case RoadDirection.North:
                            return RouteEvent.Straight;
                        case RoadDirection.East:
                            return RouteEvent.Right;
                        case RoadDirection.West:
                            return RouteEvent.Left;
                        default:
                            return RouteEvent.None;
                    }
                default:
                    return RouteEvent.None;
            }
        }


        public override IEnumerable<RouteEventData> ReadLog(IEnumerable<IEnumerable<string>> lines)
        {
            throw new NotImplementedException();
        }
    }
}