﻿using Controller.Bicycle;
using SicknessReduction.Visual.Vignetting;
using UnityEngine;
using UnityEngine.Serialization;
using Wheels;

namespace SicknessReduction
{
    public class DynamicReductionSource : MonoBehaviour
    {
        protected virtual void Start()
        {
            if (bikeController == null) Debug.LogError("bike controller = null!");
            if (useSteer)
                steerAngleSuggestor =
                    new ValueBasedRestrictionSuggestor(steerThreshold, minValue, maxValue,
                        maxValueAtSteerAngle); //deg
            if (useSpeed)
                speedSuggestor = new ValueAndTimeBasedRestrictionSuggestor(speedThreshold, minValue,
                    maxValue, maxValueAtSpeed, 1.111f /*4kmh*/, 3);
            if (useSlope)
            {
                slopeCollider.OnSlopeChanged += (timestamp, slope) => currentSlopeDeg = slope;
                slopeSuggestor =
                    new ValueBasedRestrictionSuggestor(slopeThreshold, minValue, maxValue, maxValueAtSlope);
            }
        }

        protected virtual void Update()
        {
            if (bikeController.CurrentSpeed < minSpeedForRestriction)
            {
                currentValue = 0;
                return;
            }

            if (useSteer) steerAngleSuggestor.Value = bikeController.CurrentSteerAngle;
            if (useSpeed) speedSuggestor.UpdateValue(bikeController.CurrentSpeed);
            if (useSlope) slopeSuggestor.Value = currentSlopeDeg;

            var steerSuggestion = useSteer ? steerAngleSuggestor.Suggestion * steerMultiplier : 0f;
            var speedSuggestion = useSpeed ? speedSuggestor.Suggestion * speedMultiplier : 0f;
            var slopeSuggestion = useSlope ? slopeSuggestor.Suggestion * slopeMultiplier : 0f;

            var maxSuggestion = Mathf.Max(steerSuggestion, speedSuggestion, slopeSuggestion);
            var desiredValue = Mathf.Min(maxSuggestion, maxValue);

            var desiredChange = desiredValue - currentValue;
            var maxChange = Time.deltaTime * maxValueChangePerSecond;
            var change = Mathf.Sign(desiredChange) * Mathf.Min(Mathf.Abs(desiredChange), maxChange);

            currentValue = Mathf.Clamp(currentValue + change, 0, 1);

            //Debug.Log($"SuggestionValue = {currentValue}");
        }

        #region vars

        public RbBicycleController bikeController;
        public LerpSlopeCollider slopeCollider;

        [Header("Configuration")] public bool useSpeed = true;
        public bool useSteer = true;
        public bool useSlope = true;
        public float minSpeedForRestriction = 0.28f;

        public float speedMultiplier = 1f;
        public float slopeMultiplier = 1f;
        public float steerMultiplier = 1f;

        [Header("Restriction Data")] public float minValue = 0.3f;
        public float maxValue = 0.7f;
        public float maxValueChangePerSecond = 0.8f;

        [FormerlySerializedAs("threshold")] [Tooltip("Depending on Vignetting source -> deg or deg/s")]
        public float steerThreshold = 4f;

        public float speedThreshold = 1f;
        public float slopeThreshold = 1f;

        public float maxValueAtSpeed = 8.33f; //30kmh
        public float maxValueAtSteerAngle = 15f;
        public float maxValueAtSlope = 8.531f;

        private float currentSlopeDeg;

        private ValueBasedRestrictionSuggestor steerAngleSuggestor;
        private ValueBasedRestrictionSuggestor slopeSuggestor;
        private ValueAndTimeBasedRestrictionSuggestor speedSuggestor;

        protected float currentValue;

        public float CurrentValue => currentValue;

        #endregion
    }
}