﻿using System.Threading.Tasks;
using Controller.Bicycle;
using UnityEngine;

namespace SicknessReduction.Haptic
{
    [RequireComponent(typeof(DynamicReductionSource))]
    public class DeskFanController : EspController
    {
        private const int MAX_FAN_VALUE = 95; //in %. Sometimes 100% doesn't work correctly

        private const int
            MIN_FAN_VALUE =
                25; //everything below 35 will not be enough to let the fan spin; a bit of buffer for not starting to high

        private const string TOPIC = "DeskFan/Control";

        public float maxValueAtSpeed = 6.94f; //25km/h
        public float speedUntilNoFan = 1.94f; //2kmh
        public RbBicycleController bicycleController;

        public bool useReductionSource = true;
        private int previousFanValue;
        private DynamicReductionSource reductionSource;


        protected override void Start()
        {
            base.Start();
            reductionSource = GetComponent<DynamicReductionSource>();
        }

        protected override async void Update()
        {
            base.Update();
            if (!DoUpdate || PreviousUpdateActive) return;
            PreviousUpdateActive = true;

            var cycle = useReductionSource ? FanCycleForReductionSource() : FanCycleForSpeed();
            await SendFanValue(cycle);

            PreviousUpdateActive = false;
        }

        private int FanCycleForReductionSource()
        {
            var reductionValue = reductionSource.CurrentValue;
            return (int) Mathf.Lerp(MIN_FAN_VALUE, MAX_FAN_VALUE, reductionValue);
        }

        private async Task SendFanValue(int value)
        {
            if (previousFanValue != value)
            {
                previousFanValue = value;
                await Broker.Publish(TOPIC, $"{value}");
            }
        }

        private int FanCycleForSpeed()
        {
            var speed = bicycleController.CurrentSpeed;
            if (speed <= speedUntilNoFan) return 0;
            return (int) Mathf.Lerp(MIN_FAN_VALUE, MAX_FAN_VALUE, speed / maxValueAtSpeed);
        }
    }
}