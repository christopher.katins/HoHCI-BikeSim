﻿using UnityEngine;

namespace SicknessReduction.Haptic
{
    public abstract class EspController : MonoBehaviour
    {
        protected DataBroker Broker;
        private bool brokerAvailable;
        private float lastTs;
        protected bool PreviousUpdateActive;

        private readonly float publishWaitTime = 0.05f; //20Hz
        protected bool DoUpdate { private set; get; }

        protected virtual async void Start()
        {
            Broker = DataBroker.Instance;
            await Broker.AwaitServerStarted();
            brokerAvailable = true;
        }

        protected virtual void Update()
        {
            if (!brokerAvailable)
            {
                DoUpdate = false;
                return;
            }

            var t = Time.time;
            if (t - lastTs >= publishWaitTime)
            {
                lastTs = t;
                DoUpdate = true;
            }
            else
            {
                DoUpdate = false;
            }
        }


        protected virtual void OnDestroy()
        {
            DataBroker.DisposeInstance();
        }
    }
}