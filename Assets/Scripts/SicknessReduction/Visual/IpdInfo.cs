﻿using UnityEngine;
using Valve.VR;

namespace SicknessReduction.Visual
{
    public class IpdInfo : MonoBehaviour
    {
        public delegate void OnIpdChangedEvent(float ipdMeters);

        private SteamVR_Events.Action ipdChangedAction;

        private void OnEnable()
        {
            SteamVR_Events.DeviceConnected.Listen(OnDeviceConnected);
            ipdChangedAction = SteamVR_Events.SystemAction(EVREventType.VREvent_IpdChanged, OnIpdChanged);
            ipdChangedAction.enabled = true;
        }

        private void OnDisable()
        {
            ipdChangedAction.enabled = false;
        }

        public event OnIpdChangedEvent onIpdChanged; // data event

        private void OnIpdChanged(VREvent_t vrEvent)
        {
            var ipdMeters = vrEvent.data.ipd.ipdMeters;
            onIpdChanged?.Invoke(ipdMeters);
            Debug.Log($"IPD changed to {ipdMeters}");
        }

        private void OnDeviceConnected(int index, bool connected)
        {
            if (index < 0) return;
            if (!connected || OpenVR.System == null) return;

            var deviceClass = OpenVR.System.GetTrackedDeviceClass((uint) index);
            if (deviceClass != ETrackedDeviceClass.HMD) return;

            var error = ETrackedPropertyError.TrackedProp_Success;
            var ipdMeters = OpenVR.System.GetFloatTrackedDeviceProperty((uint) index,
                ETrackedDeviceProperty.Prop_UserIpdMeters_Float, ref error);


            if (error != ETrackedPropertyError.TrackedProp_Success)
            {
                Debug.LogError($"Could not get ipd for HMD at index {index}");
            }
            else
            {
                onIpdChanged?.Invoke(ipdMeters);
                Debug.Log($"Initial IPD = {ipdMeters}");
            }

            Debug.Log("Controller got connected at index:" + index);
        }
    }
}