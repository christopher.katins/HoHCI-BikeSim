﻿using System.Linq;
using SicknessReduction.Visual.Rendering;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace SicknessReduction.Visual.Vignetting
{
    public class DynamicVignetting : DynamicReductionSource
    {
        //TODO: cite https://www.researchgate.net/publication/326760789_Assessing_vignetting_as_a_means_to_reduce_VR_sickness_during_amplified_head_rotations
        //TODO: there is a patent for this https://patents.google.com/patent/US9645395B2/en
        //TODO: check Fernandes & Feiner


        [Header("Unity Objects")] public ForwardRendererData forwardRenderer;
        public Transform hmd;

        [Range(0, 1)] public float distInnerOuterRadius = .05f;

        //TODO: figure out what angular velocity means in my context
        private MaterialBlitFeature blitFeature;
        private bool blitFeatureAvailable;
        private Material blitFeatureMaterial;
        private static readonly int OFOV = Shader.PropertyToID("_OFOV");
        private static readonly int IFOV = Shader.PropertyToID("_IFOV");

        protected override void Start()
        {
            blitFeature = (MaterialBlitFeature) forwardRenderer.rendererFeatures.FirstOrDefault(f =>
                f is MaterialBlitFeature && f.name.Equals("VignetteBlitFilter"));
            blitFeatureAvailable = blitFeature != null;

            if (!blitFeatureAvailable)
            {
                Debug.LogError("VignetteBlitFilter not available!");
                return;
            }

            ;

            // ReSharper disable once PossibleNullReferenceException
            blitFeature.SetActive(true);
            blitFeatureMaterial = blitFeature.settings.MaterialToBlit;

            base.Start();
        }

        protected override void Update()
        {
            if (!blitFeatureAvailable) return;
            base.Update();

            if (currentValue > 0)
            {
                blitFeature.SetActive(true);
                UpdateMaterial();
            }
            else
            {
                blitFeature.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            if (!blitFeatureAvailable) return;
            blitFeature.SetActive(false);
        }

        private void UpdateMaterial()
        {
            var r = Mathf.Clamp(1 - currentValue, 0, 1);
            blitFeatureMaterial.SetFloat(OFOV, r);
            blitFeatureMaterial.SetFloat(IFOV, Mathf.Clamp(r - distInnerOuterRadius, 0, 1));
        }
    }
}