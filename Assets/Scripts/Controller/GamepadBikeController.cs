﻿using Controller.Bicycle;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controller
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(IBicycleController))]
    public class GamepadBikeController : MonoBehaviour
    {
        public bool useSpeed;
        public bool useSteer;
        public bool useLean;
        public bool fixedSpeed;

        public float speedMultiplier = 200f;
        public float leanMultiplier = 20f;
        public float steerMultiplier = 15f;
        private float acceleration;

        private readonly float accelerationLoss = 0.5f;
        private IBicycleController bicycleController;
        private float lean;

        private float speed;
        private float steer;

        private void Start()
        {
            bicycleController = GetComponent<IBicycleController>();
        }

        private void Update()
        {
            if (useSteer) bicycleController.CurrentSteerAngle = steer;
            if (useLean) bicycleController.CurrentLeaningAngle = lean;
            if (useSpeed)
            {
                speed += acceleration * Time.deltaTime;
                bicycleController.CurrentSpeed = speed;
            }
        }

        [UsedImplicitly]
        public void OnSpeed(InputValue value)
        {
            acceleration = value.Get<float>() * speedMultiplier;
            if (!fixedSpeed && acceleration < 0.1f && acceleration >= 0) acceleration = -accelerationLoss;
        }

        [UsedImplicitly]
        public void OnLean(InputValue value)
        {
            lean = value.Get<float>() * leanMultiplier;
        }

        [UsedImplicitly]
        public void OnSteer(InputValue value)
        {
            steer = value.Get<float>() * steerMultiplier;
        }
    }
}