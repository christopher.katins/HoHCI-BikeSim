﻿using Controller.Bicycle;
using UnityEngine;
using Valve.VR;

namespace Controller
{
    [RequireComponent(typeof(IBicycleController))]
    public class ViveBikeController : MonoBehaviour
    {
        public SteamVR_Action_Pose steerPose;
        public float multiplier = 40f;

        private IBicycleController bicycleController;

        // Start is called before the first frame update
        private void Start()
        {
            bicycleController = GetComponent<IBicycleController>();
        }

        // Update is called once per frame
        private void Update()
        {
            var rot = steerPose.localRotation.y;
            bicycleController.CurrentSteerAngle = rot * multiplier;
        }
    }
}