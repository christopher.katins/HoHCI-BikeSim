﻿using System;
using UnityEngine;

namespace Controller.Bicycle
{
    [Serializable]
    public struct BicycleControllerMode
    {
        public float weightSteering;
        public float weightLeaning;
    }

    public interface IBicycleController
    {
        BicycleControllerMode ControllerMode { get; set; }

        float CurrentSpeed { get; set; }

        float CurrentLeaningAngle { get; set; }

        float CurrentSteerAngle { get; set; }

        Vector3 RigidBodyVelocity { get; }
    }
}