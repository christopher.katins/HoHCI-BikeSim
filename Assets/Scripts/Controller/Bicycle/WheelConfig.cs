﻿using System;
using UnityEngine;

namespace Controller.Bicycle
{
    [Serializable]
    public class WheelConfig
    {
        public WheelCollider frontLeft;
        public WheelCollider frontRight;
        public WheelCollider rearLeft;
        public WheelCollider rearRight;

        public WheelCollider[] AllWheels => new[] {frontLeft, frontRight, rearLeft, rearLeft};

        public void AdjustToGameObjects(Transform frontWheel, Transform rearWheel, float offset)
        {
            var x0 = Vector3.one - Vector3.right;

            var posFront = frontWheel.localPosition;
            var newXLeftFront = posFront.x - offset;
            var newXRightFront = posFront.x + offset;

            var posRear = rearWheel.localPosition;
            var newXLeftRear = posRear.x - offset;
            var newXRightRear = posRear.x + offset;

            var transform = frontLeft.transform;
            var localPosition = transform.localPosition;
            var newY = localPosition.y;
            var newZFront = localPosition.z;
            var newZRear = rearLeft.transform.localPosition.z;

            SetNewCoords(transform, newXLeftFront, newY, newZFront);
            SetNewCoords(frontRight.transform, newXRightFront, newY, newZFront);
            SetNewCoords(rearLeft.transform, newXLeftRear, newY, newZRear);
            SetNewCoords(rearRight.transform, newXRightRear, newY, newZRear);
        }

        private void SetNewCoords(Transform transform, float newX, float newY, float newZ)
        {
            transform.localPosition = new Vector3(newX, newY, newZ);
        }
    }
}