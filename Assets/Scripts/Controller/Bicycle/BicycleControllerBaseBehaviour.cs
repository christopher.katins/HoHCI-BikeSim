﻿using UnityEngine;

namespace Controller.Bicycle
{
    public class BicycleControllerBaseBehaviour : MonoBehaviour
    {
        [Header("Base - Config")] public BicycleControllerMode controllerMode;

        [Header("Base - Physics")] public Transform centerOfMass;
        public Rigidbody rigidBody;

        [Header("Base - Values")] public float maxLeaningAngle = 35f;

        public float maxSteeringAngle = 80f;

        [Range(0, 15)] [Tooltip("Speed in m/s")]
        public float maxSpeed = 11.111f; //40 km/h
    }
}