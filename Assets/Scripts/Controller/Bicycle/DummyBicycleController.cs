﻿using UnityEngine;

namespace Controller.Bicycle
{
    public class DummyBicycleController : MonoBehaviour, IBicycleController
    {
        public BicycleControllerMode ControllerMode { get; set; }
        public float CurrentSpeed { get; set; }
        public float CurrentLeaningAngle { get; set; }
        public float CurrentSteerAngle { get; set; }
        public Vector3 RigidBodyVelocity => Vector3.zero;
    }
}