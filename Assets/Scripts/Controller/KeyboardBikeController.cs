﻿using Controller.Bicycle;
using UnityEngine;

namespace Controller
{
    [RequireComponent(typeof(IBicycleController))]
    public class KeyboardBikeController : MonoBehaviour
    {
        public bool steer = true;
        public bool lean = true;
        public bool accelerate = true;

        public float speedIncreasePerSecond = 3f;
        public float speedDecreasePerSecond = 0.5f;
        public float brakeIncreasePerSecond = 5f;
        public float leaningAngleIncreasePerSecond = 2f;
        public float steeringAngleIncreasePerSecond = 2.5f;
        private IBicycleController bicycleController;

        private float sensedSpeed;

        private void Start()
        {
            bicycleController = GetComponent<IBicycleController>();
        }

        private void Update()
        {
            if (accelerate)
            {
                if (Input.GetKey(KeyCode.T))
                {
                    //sensedSpeed += speedIncreasePerSecond * Time.deltaTime;
                    bicycleController.CurrentSpeed = 25 / 3.6f;
                }
                else if (bicycleController.CurrentSpeed > 0)
                {
                    //sensedSpeed = Mathf.Max(0,
                    //    sensedSpeed - speedDecreasePerSecond * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.G))
                    // sensedSpeed = Mathf.Max(0,
                    //    sensedSpeed - brakeIncreasePerSecond * Time.deltaTime);
                    bicycleController.CurrentSpeed = 0f;

                //bicycleController.CurrentSpeed = sensedSpeed;
            }

            if (steer)
            {
                if (Input.GetKey(KeyCode.F))
                    bicycleController.CurrentSteerAngle -= steeringAngleIncreasePerSecond * Time.deltaTime;

                if (Input.GetKey(KeyCode.H))
                    bicycleController.CurrentSteerAngle += steeringAngleIncreasePerSecond * Time.deltaTime;

                if (Input.GetKeyUp(KeyCode.F) || Input.GetKeyUp(KeyCode.H)) bicycleController.CurrentSteerAngle = 0f;
            }

            if (lean)
            {
                if (Input.GetKey(KeyCode.R))
                    bicycleController.CurrentLeaningAngle -= leaningAngleIncreasePerSecond * Time.deltaTime;

                if (Input.GetKey(KeyCode.Z))
                    bicycleController.CurrentLeaningAngle += leaningAngleIncreasePerSecond * Time.deltaTime;

                if (Input.GetKeyUp(KeyCode.R) || Input.GetKeyUp(KeyCode.Z)) bicycleController.CurrentLeaningAngle = 0f;
            }
        }
    }
}