﻿using System;
using Sensors;

namespace Controller.Lean
{
    [Serializable]
    public struct PolarRotationMapping
    {
        public float maxRight;
        public float center;
    }

    public class PolarLeanController : ILeanController
    {
        private float leanFactor;
        public PolarRotationMapping polarRotationMapping;
        private BikeSensorData sensorData;

        public float LeanAngle { get; private set; }

        private void Start()
        {
            sensorData = BikeSensorData.Instance;
            leanFactor = 90f / (polarRotationMapping.maxRight - polarRotationMapping.center);
        }

        private void Update()
        {
            var polarData = sensorData.BleData;
            if (polarData != null) LeanAngle = (polarData.Value.Acc.y - polarRotationMapping.center) * leanFactor;
        }
    }
}