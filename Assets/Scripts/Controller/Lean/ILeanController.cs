﻿namespace Controller.Lean
{
    public interface ILeanController
    {
        float LeanAngle { get; }
    }
}