﻿using Tracking;
using UnityEngine;

namespace Controller.Lean
{
    public class HmdLeanController : MonoBehaviour, ILeanController
    {
        public CameraTracker cameraTracker;

        public float LeanAngle => cameraTracker.LeanRotation;
    }
}