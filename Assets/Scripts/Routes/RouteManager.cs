﻿using System.Linq;
using Controller.Bicycle;
using UnityEngine;

namespace Routes
{
    public class RouteManager : MonoBehaviour
    {
        public Route[] routes;
        public int selectedRoute;
        public int visibleLength = 3;
        public RbBicycleController bicycle;
        public Logging.Base.Logging logging;
        public GameObject showOnFinish;

        private GameObject bicycleGameObject;

        private void Start()
        {
            for (var i = 0; i < routes.Length; i++) routes[i].gameObject.SetActive(i == selectedRoute);

            routes[selectedRoute].OnStartEntered += OnStartEntered;
            routes[selectedRoute].OnFinishPassed += OnFinishPassed;

            PlaceBike();
        }

        private void PlaceBike()
        {
            var firstTurnPos = routes[selectedRoute].items.First().turn.transform.position;

            var startTransform = routes[selectedRoute].start.gameObject.transform;

            var difStartFirstTurn = firstTurnPos - startTransform.position;
            var bikeDirection = Vector3.zero;

            if (difStartFirstTurn.x > 1)
                bikeDirection = new Vector3(-1, 0, 0);
            else if (difStartFirstTurn.z > 1)
                bikeDirection = new Vector3(0, 0, -1);
            else if (difStartFirstTurn.x < -1)
                bikeDirection = new Vector3(1, 0, 0);
            else if (difStartFirstTurn.z < -1) bikeDirection = new Vector3(0, 0, 1);

            bicycleGameObject = bicycle.gameObject;
            bicycleGameObject.transform.position =
                startTransform.position + bikeDirection * 12;
            bicycleGameObject.transform.LookAt(startTransform);
        }

        private void OnFinishPassed()
        {
            //bicycle.enabled = false;
            Instantiate(showOnFinish, bicycle.transform);
            Destroy(logging.gameObject);
        }

        private void OnStartEntered()
        {
            logging.gameObject.SetActive(true);
        }
    }
}