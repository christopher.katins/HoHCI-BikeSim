﻿using System.Collections.Generic;

namespace Routes
{
    internal static class TurnDirectionMapper
    {
        private static readonly Dictionary<RoadDirection, TurnDirection> fromSouth =
            new Dictionary<RoadDirection, TurnDirection>
            {
                {RoadDirection.East, TurnDirection.Right},
                {RoadDirection.West, TurnDirection.Left}
            };

        private static readonly Dictionary<RoadDirection, TurnDirection> fromNorth =
            new Dictionary<RoadDirection, TurnDirection>
            {
                {RoadDirection.East, TurnDirection.Left},
                {RoadDirection.West, TurnDirection.Right}
            };

        private static readonly Dictionary<RoadDirection, TurnDirection> fromWest =
            new Dictionary<RoadDirection, TurnDirection>
            {
                {RoadDirection.North, TurnDirection.Left},
                {RoadDirection.South, TurnDirection.Right}
            };

        private static readonly Dictionary<RoadDirection, TurnDirection> fromEast =
            new Dictionary<RoadDirection, TurnDirection>
            {
                {RoadDirection.North, TurnDirection.Right},
                {RoadDirection.South, TurnDirection.Left}
            };

        public static TurnDirection GetTurnDirection(RoadDirection from, RoadDirection to)
        {
            TurnDirection direction;
            switch (from)
            {
                case RoadDirection.West:
                    return fromWest.TryGetValue(to, out direction) ? direction : TurnDirection.Straight;
                case RoadDirection.North:
                    return fromNorth.TryGetValue(to, out direction) ? direction : TurnDirection.Straight;
                case RoadDirection.East:
                    return fromEast.TryGetValue(to, out direction) ? direction : TurnDirection.Straight;
                case RoadDirection.South:
                    return fromSouth.TryGetValue(to, out direction) ? direction : TurnDirection.Straight;
                default:
                    return TurnDirection.Straight;
            }
        }
    }
}