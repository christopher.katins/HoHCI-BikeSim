﻿using UnityEngine;
using Valve.VR;

namespace Calibration
{
    public class BikeReset : MonoBehaviour
    {
        public SteamVR_Action_Boolean resetPosition;
        private Transform bikeTransform;

        private Vector3 initialPosition;
        private Quaternion initialRotation;

        private void Start()
        {
            bikeTransform = transform;
            initialPosition = bikeTransform.localPosition;
            initialRotation = bikeTransform.localRotation;
            resetPosition.AddOnStateUpListener(OnResetPosition, SteamVR_Input_Sources.Any);
        }

        private void OnResetPosition(SteamVR_Action_Boolean fromaction, SteamVR_Input_Sources fromsource)
        {
            Debug.Log("Resetting Bike Position and Rotation");
            bikeTransform.localPosition = initialPosition;
            bikeTransform.localRotation = initialRotation;
        }
    }
}