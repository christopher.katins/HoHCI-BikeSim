﻿using UnityEngine;
using Valve.VR;

namespace Calibration
{
    public class PersistatePlayerPosition : MonoBehaviour
    {
        private const string KEY_PLAYER_POS_X = "player_pos_x";
        private const string KEY_PLAYER_POS_Y = "player_pos_y";
        private const string KEY_PLAYER_POS_Z = "player_pos_z";

        public SteamVR_Action_Boolean savePos;

        private Transform playerTransform;

        // Start is called before the first frame update
        private void Start()
        {
            playerTransform = GetComponent<Transform>();
            savePos.AddOnStateDownListener(OnSavePos, SteamVR_Input_Sources.Any);
            Load();
        }

        private void OnSavePos(SteamVR_Action_Boolean fromaction, SteamVR_Input_Sources fromsource)
        {
            Save();
        }

        private void Load()
        {
            if (PlayerPrefs.HasKey(KEY_PLAYER_POS_X) && PlayerPrefs.HasKey(KEY_PLAYER_POS_Y) &&
                PlayerPrefs.HasKey(KEY_PLAYER_POS_Z))
            {
                var x = PlayerPrefs.GetFloat(KEY_PLAYER_POS_X);
                var y = PlayerPrefs.GetFloat(KEY_PLAYER_POS_Y);
                var z = PlayerPrefs.GetFloat(KEY_PLAYER_POS_Z);
                playerTransform.position = new Vector3(x, y, z);
            }
        }

        public void Save()
        {
            var position = playerTransform.position;
            PlayerPrefs.SetFloat(KEY_PLAYER_POS_X, position.x);
            PlayerPrefs.SetFloat(KEY_PLAYER_POS_Y, position.y);
            PlayerPrefs.SetFloat(KEY_PLAYER_POS_Z, position.z);
            PlayerPrefs.Save();
        }
    }
}