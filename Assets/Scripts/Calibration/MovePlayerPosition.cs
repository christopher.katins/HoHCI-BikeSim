﻿using UnityEngine;
using Valve.VR;

namespace Calibration
{
    public class MovePlayerPosition : MonoBehaviour
    {
        public SteamVR_Action_Boolean resetAction;
        public SteamVR_Action_Boolean modeAction;

        public SteamVR_Action_Boolean moveForwards;
        public SteamVR_Action_Boolean moveBackwards;
        public SteamVR_Action_Boolean moveRight;
        public SteamVR_Action_Boolean moveLeft;

        public float movementSpeed = 1f;
        private bool moveModeForwards;

        private Transform playerTransform;

        private void Start()
        {
            playerTransform = GetComponent<Transform>();
            resetAction.AddOnStateUpListener(OnResetActionUp, SteamVR_Input_Sources.Any);
            modeAction.AddOnStateUpListener(OnModeSwitchUp, SteamVR_Input_Sources.Any);
        }

        private void Update()
        {
            var left = moveLeft.GetState(SteamVR_Input_Sources.Any) ? 1 : 0;
            var right = moveRight.GetState(SteamVR_Input_Sources.Any) ? 1 : 0;
            var forward = moveForwards.GetState(SteamVR_Input_Sources.Any) ? 1 : 0;
            var backward = moveBackwards.GetState(SteamVR_Input_Sources.Any) ? 1 : 0;

            var x = right - left;
            var y = moveModeForwards ? 0f : forward - backward;
            var z = moveModeForwards ? forward - backward : 0f;

            playerTransform.localPosition += new Vector3(x, y, z) * (Time.deltaTime * movementSpeed);
        }

        private void OnResetActionUp(SteamVR_Action_Boolean fomAction, SteamVR_Input_Sources fromSource)
        {
            OpenVR.Chaperone.ResetZeroPose(ETrackingUniverseOrigin.TrackingUniverseStanding);
        }

        private void OnModeSwitchUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            moveModeForwards = !moveModeForwards;
        }
    }
}