﻿namespace Roads
{
    public interface IRoad
    {
        float SlopeDeg { get; }

        float MinY { get; }
    }
}