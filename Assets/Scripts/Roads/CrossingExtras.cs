﻿using System;
using Routes;
using UnityEngine;

namespace Roads
{
    [Serializable]
    public struct CrossingData
    {
        public Transform west;
        public Transform north;
        public Transform east;
        public Transform south;
    }

    public class CrossingExtras : Turn
    {
        public CrossingData crossingData;

        protected override Transform RoadDirectionToTransform(RoadDirection position)
        {
            switch (position)
            {
                case RoadDirection.North:
                    return crossingData.north;
                case RoadDirection.West:
                    return crossingData.west;
                case RoadDirection.East:
                    return crossingData.east;
                case RoadDirection.South:
                    return crossingData.south;
                case RoadDirection.None:
                    throw new ArgumentException("RoadDirection.None not allowed for adding arrows");
                default:
                    throw new ArgumentOutOfRangeException(nameof(position), position, "Wrong Argument for AddArrows");
            }
        }
    }
}