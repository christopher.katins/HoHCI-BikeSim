﻿using UnityEngine;

public class StreetPartMetaTag : MonoBehaviour
{
    public const string TAG_EVEN = "street_even";
    public const string TAG_UNEVEN = "street_uneven";

    private const float SLOPE_THRES = .5f;

    private void Awake()
    {
        var slope = transform.parent.localRotation.eulerAngles.z;
        tag = Mathf.Abs(slope) <= SLOPE_THRES ? TAG_EVEN : TAG_UNEVEN;
    }
}