﻿using Routes;
using UnityEngine;

namespace Roads
{
    [RequireComponent(typeof(BoxCollider))]
    public class StraightRoadExtras : MonoBehaviour, IRoad
    {
        public delegate void OnArcEnteredEvent();

        public delegate void OnArcPassedEvent();

        public GameObject arcPrefab;

        private GameObject arc;
        private bool hasArc;
        public TriggerState ArcState { get; private set; } = TriggerState.Outside;

        private void Start()
        {
            SlopeDeg = transform.localRotation.eulerAngles.z;
            MinY = GetComponent<BoxCollider>().bounds.center.y;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("bike"))
                if (hasArc)
                {
                    OnArcEntered?.Invoke();
                    ArcState = TriggerState.Inside;
                }
        }

        private void OnTriggerExit(Collider other)
        {
            if (hasArc && other.CompareTag("bike"))
            {
                Destroy(arc);
                ArcState = TriggerState.Outside;
                OnArcPassed?.Invoke();
            }
        }

        public float SlopeDeg { get; private set; }

        public float MinY { get; private set; }

        public event OnArcEnteredEvent OnArcEntered;
        public event OnArcPassedEvent OnArcPassed;

        public void ShowArc()
        {
            arc = Instantiate(arcPrefab, transform);
            hasArc = arc != null;
        }
    }
}