﻿using Controller.Bicycle;
using UnityEngine;

namespace Roads
{
    [RequireComponent(typeof(StraightRoadExtras))]
    public class SlopeExtras : MonoBehaviour
    {
        private const float TOLERANCE_ANGLE = 1f;

        public bool firstOrLast;
        private BikeSpeedBooster booster;
        private bool isFirst;

        private Vector3 lowerPos;

        private int slopePrefix;

        private StraightRoadExtras straightRoadExtras;
        private Vector3 upperPos;

        private void OnEnable()
        {
            straightRoadExtras = GetComponent<StraightRoadExtras>();
            // the origin of a road tile is always on the lower end.
            var t = transform;
            lowerPos = t.position;
            upperPos = lowerPos + t.right * t.localScale.x;
            booster = BikeSpeedBooster.Instance;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("bike"))
            {
                var bikePos = other.ClosestPointOnBounds(lowerPos);
                if ((bikePos - lowerPos).magnitude < (bikePos - upperPos).magnitude)
                    //coming from lower pos -> uphill
                    slopePrefix = 1;
                else
                    slopePrefix = -1;

                var directedSlope = slopePrefix * straightRoadExtras.SlopeDeg;
                booster.SetFixedBoostForSlope(directedSlope);

                isFirst = other.transform.rotation.eulerAngles.x < TOLERANCE_ANGLE;
                if (firstOrLast && isFirst) booster.AddBoosterForFirstAndSlope(directedSlope);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            //Debug.Log($"EXIT bike rotation = {other.transform.rotation.eulerAngles}");

            if (other.CompareTag("bike") && firstOrLast)
            {
                var isLast = !isFirst && other.transform.rotation.eulerAngles.x > TOLERANCE_ANGLE;
                if (!isLast) return;
                booster.AddBoosterForLastAndSlope(slopePrefix * straightRoadExtras.SlopeDeg);
                booster.ResetFixedBoost(); //on straight road again
            }
        }
    }
}