﻿using System;
using JetBrains.Annotations;
using Routes;
using UnityEngine;

namespace Roads
{
    [Serializable]
    public struct JunctionData
    {
        [SerializeField] private Transform north;

        [SerializeField] private Transform east;

        [SerializeField] private Transform south;

        [CanBeNull]
        public Transform GetEast(float yRotation)
        {
            var rotation = (int) yRotation;
            switch (rotation)
            {
                case 0:
                    return east;
                case 90:
                    return north;
                case 270:
                    return south;
                default:
                    return null;
            }
        }

        [CanBeNull]
        public Transform GetNorth(float yRotation)
        {
            var rotation = (int) yRotation;
            switch (rotation)
            {
                case 0:
                    return north;
                case 180:
                    return south;
                case 270:
                    return east;
                default:
                    return null;
            }
        }

        [CanBeNull]
        public Transform GetSouth(float yRotation)
        {
            var rotation = (int) yRotation;
            switch (rotation)
            {
                case 0:
                    return south;
                case 90:
                    return east;
                case 180:
                    return north;
                default:
                    return null;
            }
        }

        [CanBeNull]
        public Transform GetWest(float yRotation)
        {
            var rotation = (int) yRotation;
            switch (rotation)
            {
                case 90:
                    return south;
                case 180:
                    return east;
                case 270:
                    return north;
                default:
                    return null;
            }
        }
    }

    public class JunctionExtras : Turn
    {
        public JunctionData junctionData;

        protected override Transform RoadDirectionToTransform(RoadDirection position)
        {
            var rotation = transform.rotation.eulerAngles.y;
            switch (position)
            {
                case RoadDirection.North:
                    return junctionData.GetNorth(rotation);
                case RoadDirection.West:
                    return junctionData.GetWest(rotation);
                case RoadDirection.East:
                    return junctionData.GetEast(rotation);
                case RoadDirection.South:
                    return junctionData.GetSouth(rotation);
                case RoadDirection.None:
                    throw new ArgumentException("RoadDirection.None not allowed for adding arrows");
                default:
                    throw new ArgumentOutOfRangeException(nameof(position), position, "Wrong Argument for AddArrows");
            }
        }
    }
}