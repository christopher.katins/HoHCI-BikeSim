﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Debugging
{
    public class ObjectAndMaterialCounter : MonoBehaviour
    {
        public float checkInterval = 1.5f;
        private Camera cam;

        private float lastCheckTime;

        private MeshRenderer[] allRenderers;

        private int visibleCounter = 0;
        private int objectsInBatches = 0;
        private int objectsNotInBatches = 0;
        private HashSet<string> materials = new HashSet<string>();

        private void OnEnable()
        {
            cam = Camera.main;
            allRenderers = FindObjectsOfType<MeshRenderer>();
        }

        private void OnGUI()
        {
            GUI.TextField(new Rect(20, 120, 200, 100),
                $"Visible Objects: {visibleCounter}\nObjects in batches: {objectsInBatches}\nObjects not in batches: {objectsNotInBatches}\n#Materials visible: {materials.Count}");
            GUI.TextField(new Rect(20, 220, 300, 400), $"Materials: {string.Join("\n  -", materials)}");
        }

        private void Update()
        {
            if (Time.time - lastCheckTime < checkInterval) return;

            objectsInBatches = 0;
            objectsNotInBatches = 0;
            visibleCounter = 0;
            materials.Clear();

            lastCheckTime = Time.time;

            foreach (var meshRenderer in allRenderers)
            {
                if (meshRenderer.isVisible)
                {
                    visibleCounter++;
                }

                if (meshRenderer.isPartOfStaticBatch)
                {
                    objectsInBatches++;
                }
                else
                {
                    objectsNotInBatches++;
                }

                foreach (var m in meshRenderer.materials)
                {
                    materials.Add(m.name);
                }
            }
        }
    }
}