﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using AdditionalMathf;
using UnityEngine;

public class Helpers
{
    public static void DrawPlane(Vector3 position, Vector3 normal)
    {
        Vector3 v3;

        if (normal.normalized != Vector3.forward)
            v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
        else
            v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude;

        var corner0 = position + v3;
        var corner2 = position - v3;
        var q = Quaternion.AngleAxis(90.0f, normal);
        v3 = q * v3;
        var corner1 = position + v3;
        var corner3 = position - v3;

        Debug.DrawLine(corner0, corner2, Color.green);
        Debug.DrawLine(corner1, corner3, Color.green);
        Debug.DrawLine(corner0, corner1, Color.green);
        Debug.DrawLine(corner1, corner2, Color.green);
        Debug.DrawLine(corner2, corner3, Color.green);
        Debug.DrawLine(corner3, corner0, Color.green);
        Debug.DrawRay(position, normal, Color.red);
    }

    public static void DrawLine(Vector3 p1, Vector3 p2, float width)
    {
        var count = 1 + Mathf.CeilToInt(width); // how many lines are needed.
        if (count == 1)
        {
            Gizmos.DrawLine(p1, p2);
        }
        else
        {
            var c = Camera.current;
            if (c == null)
            {
                Debug.LogError("Camera.current is null");
                return;
            }

            var scp1 = c.WorldToScreenPoint(p1);
            var scp2 = c.WorldToScreenPoint(p2);

            var v1 = (scp2 - scp1).normalized; // line direction
            var n = Vector3.Cross(v1, Vector3.forward); // normal vector

            for (var i = 0; i < count; i++)
            {
                var o = 0.99f * n * width * ((float) i / (count - 1) - 0.5f);
                var origin = c.ScreenToWorldPoint(scp1 + o);
                var destiny = c.ScreenToWorldPoint(scp2 + o);
                Gizmos.DrawLine(origin, destiny);
            }
        }
    }

    public static float GetMinComponent(Vector3 vector)
    {
        if (vector.x <= vector.y && vector.x <= vector.z) return vector.x;
        if (vector.y <= vector.x && vector.y <= vector.z) return vector.y;
        return vector.z;
    }

    public static float GetMaxComponent(Vector3 vector)
    {
        if (vector.x > vector.y && vector.x > vector.z) return vector.x;
        if (vector.y > vector.x && vector.y > vector.z) return vector.y;
        return vector.z;
    }

    public static long RoundToLong(float f)
    {
        return (long) Mathf.Round(f);
    }

    public static Vector3 Vector3Abs(Vector3 value)
    {
        return new Vector3(Mathf.Abs(value.x), Mathf.Abs(value.y), Mathf.Abs(value.z));
    }

    /// <summary>
    ///     Remove outliers using the interquartile range method
    /// </summary>
    /// <param name="collection">A collection of floats, from which outliers are to be removed</param>
    /// <returns>A collection without the outliers</returns>
    public static IEnumerable<float> RemoveOutliers(IList<float> collection)
    {
        if (collection.Count < 4) return collection;
        var quartiles = new Quartiles(collection);
        var range = 1.5f * quartiles.Iqr;

        // see https://www.statology.org/outliers-excel/
        return collection.Where(v => v >= quartiles.Q1 - range && v <= quartiles.Q3 + range);
    }

    public static IPAddress GetIPAddress()
    {
        IPAddress usedIp = null;

        foreach (var ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                if (usedIp == null || ip.ToString().StartsWith("192.168.1."))
                    usedIp = ip;

        return usedIp;
    }
}