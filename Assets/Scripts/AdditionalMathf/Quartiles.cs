﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AdditionalMathf
{
    public class Quartiles
    {
        public Quartiles(IList<float> list)
        {
            if (list.Count < 4) throw new ArgumentException("list must not be empty");
            var sorted = list.OrderBy(v => v).ToList();

            var i1 = 0.25f * list.Count;
            var i2 = 0.5f * list.Count;
            var i3 = 0.75f * list.Count;

            Q1 = ValueForFloatingIndex(sorted, i1);
            Q2 = ValueForFloatingIndex(sorted, i2);
            Q3 = ValueForFloatingIndex(sorted, i3);
            Iqr = Q3 - Q1;
        }

        public float Q1 { get; }
        public float Q2 { get; }
        public float Q3 { get; }

        public float Iqr { get; }

        private float ValueForFloatingIndex(IList<float> list, float index)
        {
            var i = Mathf.Max(0, index - 1);
            if (i % 1f == 0) return list[(int) i];

            var flooredIndex = Mathf.FloorToInt(i);
            var lower = list[flooredIndex];
            var higher = list[Mathf.Min(Mathf.CeilToInt(i), list.Count - 1)];

            return Mathf.Lerp(lower, higher, i - flooredIndex);
        }
    }
}