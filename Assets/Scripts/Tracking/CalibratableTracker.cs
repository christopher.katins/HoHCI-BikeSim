﻿using UnityEngine;
using Valve.VR;

namespace Tracking
{
    public abstract class CalibratableTracker : MonoBehaviour
    {
        public Transform bicycleTransform;

        private Transform trackerTransform;
        private Vector3 zeroPos;
        private Vector3 zeroRot;
        protected abstract string KeyPrefix { get; }

        private string KeyPosX => $"{KeyPrefix}_vive_tracker_pos_x";

        private string KeyPosY => $"{KeyPrefix}_vive_tracker_pos_y";
        private string KeyPosZ => $"{KeyPrefix}_vive_tracker_pos_z";
        private string KeyRotX => $"{KeyPrefix}_vive_tracker_rot_x";
        private string KeyRotY => $"{KeyPrefix}_vive_tracker_rot_y";
        private string KeyRotZ => $"{KeyPrefix}_vive_tracker_rot_z";

        public Vector3 RelativeRotation
        {
            get
            {
                if (trackerTransform == null) return Vector3.zero; //TODO: something smarter
                var newVal = trackerTransform.localRotation.eulerAngles - zeroRot;
                var x = newVal.x;
                var y = newVal.y;
                var z = newVal.z;
                if (x > 180) x = -(360 - x);
                if (y > 180) y = -(360 - y);
                if (z > 180) z = -(360 - z);

                if (x < -180) x = -(-360 - x);
                if (y < -180) y = -(-360 - y);
                if (z < -180) z = -(-360 - z);
                return new Vector3(x, y, z);
            }
        }

        public Vector3 RelativePosition =>
            trackerTransform == null ? Vector3.zero : trackerTransform.localPosition - zeroPos;

        public Vector3 ZeroRot => zeroRot;
        public Vector3 ZeroPos => zeroPos;

        protected virtual void Start()
        {
            trackerTransform = transform;
            if (PlayerPrefs.HasKey(KeyPosX) && PlayerPrefs.HasKey(KeyPosY) && PlayerPrefs.HasKey(KeyPosZ))
            {
                var x = PlayerPrefs.GetFloat(KeyPosX);
                var y = PlayerPrefs.GetFloat(KeyPosY);
                var z = PlayerPrefs.GetFloat(KeyPosZ);

                zeroPos = new Vector3(x, y, z);
            }

            if (PlayerPrefs.HasKey(KeyRotX) && PlayerPrefs.HasKey(KeyRotY) && PlayerPrefs.HasKey(KeyRotZ))
            {
                var x = PlayerPrefs.GetFloat(KeyRotX);
                var y = PlayerPrefs.GetFloat(KeyRotY);
                var z = PlayerPrefs.GetFloat(KeyRotZ);

                zeroRot = new Vector3(x, y, z);
            }
        }

        //sets current position as zero -> straighten handlebar and call this function
        public void Calibrate()
        {
            zeroRot = trackerTransform.localRotation.eulerAngles;
            zeroPos = trackerTransform.localPosition;
            PlayerPrefs.SetFloat(KeyRotX, zeroRot.x);
            PlayerPrefs.SetFloat(KeyRotY, zeroRot.y);
            PlayerPrefs.SetFloat(KeyRotZ, zeroRot.z);
            PlayerPrefs.SetFloat(KeyPosX, zeroPos.x);
            PlayerPrefs.SetFloat(KeyPosY, zeroPos.y);
            PlayerPrefs.SetFloat(KeyPosZ, zeroPos.z);
            PlayerPrefs.Save();
        }

        public void OnConnectedChanged(SteamVR_Action_Pose pose, SteamVR_Input_ActionScopes sources, bool connected)
        {
        }

        public void OnConnectedChanged()
        {
            Debug.Log("connected changed");
        }
    }
}