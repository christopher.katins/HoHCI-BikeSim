﻿using Valve.VR;

namespace Tracking
{
    public interface IViveTracker
    {
        string Id { get; }

        SteamVR_TrackedObject TrackedObject { get; }
        float BatteryLevel { get; set; }
    }
}