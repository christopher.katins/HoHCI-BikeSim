﻿using UnityEngine;

namespace Tracking
{
    public class CameraTracker : CalibratableTracker
    {
        public KineticLegTracker legTracker;
        public float LeanRotation { get; private set; }

        protected override string KeyPrefix => "cam";

        private void Update()
        {
            var adjustedRotation = RelativeRotation - legTracker.RelativeRotation;
            LeanRotation = -adjustedRotation.z; //left negative, right positive
        }

        /*private void OnGUI()
        {
            GUI.TextArea(new Rect(0, 400, 200, 90),
                $"LeanRotation: {LeanRotation}\nRelativeRotation: {RelativeRotation}\nRelativePosition: {RelativePosition}");
        }*/
    }
}