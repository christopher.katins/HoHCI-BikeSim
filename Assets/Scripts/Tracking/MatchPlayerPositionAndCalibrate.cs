﻿using System.Collections;
using Display;
using UnityEngine;
using Valve.VR;

namespace Tracking
{
    public enum CalibrationMode
    {
        None,
        MatchOnly,
        AutoImmediate,
        AutoCountdown
    }

    public class MatchPlayerPositionAndCalibrate : MonoBehaviour
    {
        public Transform player;
        public CalibrationMode calibrationMode;
        public CountdownDisplay countdownHud;


        [Header("Front Wheel")] public Transform frontWheelDesiredPosition;
        public FrontWheelTracker frontWheelTracker;

        [Header("Trainer Leg")] public Transform legDesiredPosition;
        public KineticLegTracker legTracker;

        [Header("HMD")] public bool calibrateHmd = true;
        public LookStraightDisplay lookStraightHud;
        public CameraTracker hmdTracker;
        public Transform bikeTransform;
        private bool autoSet;

        private SteamVR_TrackedObject fwTrackedObject;

        private Transform fwtTransform;
        private SteamVR_TrackedObject legTrackedObject;
        private Transform legTransform;

        private int matching;
        private Transform playerTransform;

        private void Start()
        {
            fwtTransform = frontWheelTracker.transform;
            legTransform = legTracker.transform;
            playerTransform = player.transform;

            fwTrackedObject = legTracker.GetComponent<SteamVR_TrackedObject>();
            legTrackedObject = frontWheelTracker.GetComponent<SteamVR_TrackedObject>();
        }

        private void Update()
        {
            AutoCalibration();
            DoMatching();
        }

        public void Match()
        {
            matching = 1;
        }

        private void MatchFrontWheel()
        {
            var dif = frontWheelDesiredPosition.position - fwtTransform.position;
            playerTransform.position += dif;

            StartCoroutine(HideFrontWheelTracker());
        }

        private void MatchLeg()
        {
            //The only way to match it now is by rotating
            var fwtPosition = fwtTransform.position;
            var fwtToDesiredLeg = legDesiredPosition.position - fwtPosition;
            var fwtToRealLeg = legTransform.position - fwtPosition;

            var dl2d = new Vector2(fwtToDesiredLeg.x, fwtToDesiredLeg.z);
            var dr2d = new Vector2(fwtToRealLeg.x, fwtToRealLeg.z);

            var angle = Vector2.Angle(dl2d, dr2d);
            playerTransform.RotateAround(fwtPosition, playerTransform.up, -angle);

            StartCoroutine(HideLegTracker());
        }

        private IEnumerator HideFrontWheelTracker()
        {
            yield return new WaitForSeconds(1);
            frontWheelDesiredPosition.gameObject.SetActive(false);
            frontWheelTracker.GetComponent<MeshRenderer>().enabled = false;
        }

        private IEnumerator HideLegTracker()
        {
            yield return new WaitForSeconds(1);
            legDesiredPosition.gameObject.SetActive(false);
            legTracker.GetComponent<MeshRenderer>().enabled = false;
        }

        private void AutoCalibration()
        {
            if (calibrationMode == CalibrationMode.None || autoSet || !fwTrackedObject.isValid ||
                !legTrackedObject.isValid) return;

            if (calibrationMode == CalibrationMode.AutoCountdown)
            {
                var display = Instantiate(countdownHud);
                display.OnCountdownDone = () =>
                {
                    Match();
                    return "Calibrated!";
                };
                autoSet = true;
            }
            else
            {
                Match();
                autoSet = true;
            }
        }

        private void DoMatching()
        {
            if (matching == 1)
            {
                MatchFrontWheel();
                matching++;
            }
            else if (matching == 2)
            {
                MatchLeg();
                if (calibrationMode == CalibrationMode.MatchOnly || calibrationMode == CalibrationMode.None)
                    matching = 0;
                else
                    matching++;
            }
            else if (matching == 3)
            {
                legTracker.Calibrate();
                frontWheelTracker.Calibrate();
                matching = 0;
                DoLookStraightIfWanted();
            }
        }

        private void DoLookStraightIfWanted()
        {
            if (!calibrateHmd) return;

            var lookStraightDisplay = Instantiate(lookStraightHud);
            lookStraightDisplay.OnDone += OnLookStraightDone;
            var lTransform = lookStraightDisplay.transform;
            var newPos = bikeTransform.position + bikeTransform.forward * 1.4f;
            newPos.y = hmdTracker.transform.position.y;
            lTransform.position = newPos;
        }

        private void OnLookStraightDone()
        {
            hmdTracker.Calibrate();
        }
    }
}