﻿using Valve.VR;

namespace Tracking
{
    public class KineticLegTracker : CalibratableTracker, IViveTracker
    {
        public bool calibrateOnStart = true;
        public string id;
        protected override string KeyPrefix => "kl";

        protected override void Start()
        {
            base.Start();
            if (calibrateOnStart) Calibrate();
        }

        public string Id => id;
        public SteamVR_TrackedObject TrackedObject => GetComponent<SteamVR_TrackedObject>();
        public float BatteryLevel { get; set; }
    }
}