﻿using UnityEngine;

public class IgnoreClipping : MonoBehaviour
{
    public int ignoreClippingLayer = 13;
    public float preClippingDistance = 500f;

    public int smallObjectsLayer = 14;
    public float smallObjectsDistance = 200f;

    private Camera cam;

    private void OnEnable()
    {
        cam = GetComponent<Camera>();
        applyDistances();
    }

    public void applyDistances(float? overridePreClippingDistance = null, float? overrideSmallObjectsDistance = null)
    {
        var pcD = overridePreClippingDistance ?? preClippingDistance;
        var soD = overrideSmallObjectsDistance ?? smallObjectsDistance;
        
        var distances = new float[32];
        for (var i = 0; i < distances.Length; i++) distances[i] = pcD;
        distances[ignoreClippingLayer] = 0f;
        distances[smallObjectsLayer] = soD;
        cam.layerCullDistances = distances;
    }
}