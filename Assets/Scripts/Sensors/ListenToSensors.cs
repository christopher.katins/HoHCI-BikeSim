﻿using Sensors.ANT;
using Sensors.Bluetooth;
using UnityEngine;
using UnityEngine.Serialization;

namespace Sensors
{
    public class ListenToSensors : MonoBehaviour
    {
        public SpeedSensorConfig speedSensorConfig;

        [FormerlySerializedAs("polarSensorConfig")]
        public BleSensorConfig bleSensorConfig;

        public int hrAntId;
        public int powerMeterId;

        private void Start()
        {
            BikeSensorData.Instance.StartListening(speedSensorConfig, bleSensorConfig,
                powerMeterId < 0 ? null : new int?(powerMeterId),
                hrAntId < 0 ? null : new int?(hrAntId));
        }

        private void OnDestroy()
        {
            BikeSensorData.Instance.Dispose();
        }
    }
}