﻿using Controller;
using SicknessReduction.Haptic;
using TMPro;
using Tracking;
using UnityEngine;
using Valve.VR;

namespace Sensors
{
    public class SensorStatus : MonoBehaviour
    {
        public TextMeshProUGUI speedStatusText;
        public TextMeshProUGUI hrStatusText;
        public TextMeshProUGUI powerStatusText;
        public TextMeshProUGUI polarStatusText;
        public TextMeshProUGUI mqttStatusText;

        public SensorBikeController sensorBikeController;
        public FrontWheelTracker fwTracker;
        public KineticLegTracker legTracker;

        public bool waitForTrackers = true;
        public bool waitForSpeed = true;
        public bool waitForHr = true;
        public bool waitForPolar = true;
        public bool waitForPower = true;
        public bool waitForMqtt;
        private DataBroker dataBroker;

        private SteamVR_TrackedObject fwObject;
        private SteamVR_TrackedObject legObject;
        private string mqttClient;
        private bool mqttReady;
        private string mqttTopic;

        private bool PolarReady => BikeSensorData.Instance.BleData.HasValue &&
                                   BikeSensorData.Instance.BleData.Value.Acc != Vector3.zero;

        private void Start()
        {
            fwObject = fwTracker.GetComponent<SteamVR_TrackedObject>();
            legObject = legTracker.GetComponent<SteamVR_TrackedObject>();
        }

        private void Update()
        {
            var hrData = BikeSensorData.Instance.HrData;
            var speedData = BikeSensorData.Instance.SpeedData;
            var powerData = BikeSensorData.Instance.PowermeterData;
            var polarData = BikeSensorData.Instance.BleData;
            hrStatusText.text = hrData.HasValue
                ? $"Heart Rate: Connected ({hrData?.HeartRate})"
                : "Heart Rate: Disconnected";
            speedStatusText.text =
                speedData.HasValue ? $"Speed: Connected ({speedData?.SpeedKmh}) " : "Speed: Disconnected";
            powerStatusText.text = powerData.HasValue
                ? $"Power/Cadence: Connected ({powerData?.InstantaneousPower})"
                : "Power/Cadence: Disconnected";
            polarStatusText.text = PolarReady
                ? $"Polar: Connected ({polarData?.Acc}, {polarData?.EcgValue}, {polarData?.Hr})"
                : "Polar: Disconnected";
            if (waitForMqtt)
                mqttStatusText.text = mqttReady
                    ? $"Mqtt: Connected ({mqttClient}, {mqttTopic})"
                    : "Mqtt: Disconnected";


            if (!(waitForTrackers && (!fwObject.isValid || !legObject.isValid)
                  || waitForSpeed && !speedData.HasValue
                  || waitForHr && !hrData.HasValue
                  || waitForPower && !powerData.HasValue
                  || waitForPolar && !PolarReady
                  || waitForMqtt && !mqttReady)
                )
                Destroy(gameObject);
        }

        private void OnEnable()
        {
            sensorBikeController.enabled = false;
            if (waitForMqtt)
            {
                dataBroker = DataBroker.Instance;
                dataBroker.onClientSubscribed = onClientSubscribedToTopic;
            }
            else
            {
                mqttStatusText.enabled = false;
            }
        }

        private void OnDestroy()
        {
            sensorBikeController.enabled = true;
        }

        private void onClientSubscribedToTopic(string clientName, string topic)
        {
            mqttClient = clientName;
            mqttTopic = topic;
            mqttReady = true;
        }
    }
}