﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sensors.Bluetooth
{
    public readonly struct RawPowermeterData
    {
        public readonly int instantaniousPower;
        public readonly float accumulatedTorque;
        public readonly int crankRevolutions;
        public readonly float lastCrankEventTime;

        public RawPowermeterData(int instantaniousPower, float accumulatedTorque, int crankRevolutions,
            float lastCrankEventTime)
        {
            this.lastCrankEventTime = lastCrankEventTime;
            this.crankRevolutions = crankRevolutions;
            this.accumulatedTorque = accumulatedTorque;
            this.instantaniousPower = instantaniousPower;
        }


        public static RawPowermeterData FromString(string s)
        {
            var data = s.Split(';');
            if (data.Length != 4) throw new FormatException("HRData String has wrong format");
            
            var pow = int.Parse(data[0]);
            var torque = float.Parse(data[1]);
            var crankRevs = int.Parse(data[2]);
            var lastCrankTime = float.Parse(data[3]);
            return new RawPowermeterData(pow, torque, crankRevs, lastCrankTime);
        }
    }

    public struct PowermeterData
    {
        public readonly int power;
        public readonly int cadence;
        public readonly float torque;

        public PowermeterData(int power, int cadence, float torque)
        {
            this.power = power;
            this.cadence = cadence;
            this.torque = torque;
        }

    }
}