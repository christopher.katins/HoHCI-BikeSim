﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sensors.Polar
{
    public readonly struct AccData
    {
        public long Timestamp { get; }
        public List<Vector3> Values { get; }

        private AccData(long timestamp, List<Vector3> values)
        {
            Timestamp = timestamp;
            Values = values;
        }

        public static AccData FromString(string s)
        {
            var data = s.Split(';');
            if (data.Length == 2)
            {
                var t = long.Parse(data[0]);
                var tupleStrings = data[1].Split(',').Select(item => item.Replace("(", "").Replace(")", ""));
                var floats = tupleStrings.Select(s1 => s1.Split('/').Select(value => float.Parse(value)).ToList())
                    .ToList();
                return new AccData(t, floats.Select(a => new Vector3(a[0], a[1], a[2])).ToList());
            }

            throw new FormatException("AccData String has wrong format");
        }
    }
}