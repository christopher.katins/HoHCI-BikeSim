﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sensors.Polar
{
    public readonly struct EcgData
    {
        public long Timestamp { get; }
        public List<float> Values { get; }

        private EcgData(long timestamp, List<float> values)
        {
            Timestamp = timestamp;
            Values = values;
        }

        public static EcgData FromString(string s)
        {
            var data = s.Split(';');
            if (data.Length == 2)
            {
                var t = long.Parse(data[0]);
                var values = data[1].Split(',').Select(value => float.Parse(value)).ToList();
                return new EcgData(t, values);
            }

            throw new FormatException("EcgData String has wrong format");
        }
    }
}