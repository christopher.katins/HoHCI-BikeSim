﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sensors.Bluetooth
{
    public readonly struct HRData
    {
        public List<long> RrsMs { get; }
        public int HeartRate { get; }

        private HRData(List<long> rrsMs, int heartRate)
        {
            RrsMs = rrsMs;
            HeartRate = heartRate;
        }

        public static HRData FromString(string s)
        {
            var data = s.Split(';');
            if (data.Length == 2)
            {
                var hr = int.Parse(data[0]);
                var values = data[1].Split(',').Select(value => long.Parse(value)).ToList();
                return new HRData(values, hr);
            }

            throw new FormatException("HRData String has wrong format");
        }
    }
}