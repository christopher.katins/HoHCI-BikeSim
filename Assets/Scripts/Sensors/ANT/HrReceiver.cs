﻿using System.Collections.Generic;
using ANT_Managed_Library;
using UnityEngine;

namespace Sensors.ANT
{
    public struct HrSensorData
    {
        public float HeartRate;
    }

    public class HrReceiver : AwaitDevice
    {
        private AntChannel backgroundScanChannel;
        private AntChannel deviceChannel;
        private HrSensorData sensorData;

        private bool connected;
        public override int DeviceId { get; }
        public override bool Connected => connected;

        public HrSensorData SensorData => sensorData;

        public HrReceiver()
        {
        }

        public HrReceiver(int deviceID)
        {
            DeviceId = deviceID;
        }

        public override void Connect(AntDevice device)
        {
            var channelID = AntManager.Instance.GetFreeChannelID();
            deviceChannel = AntManager.Instance.OpenChannel(ANT_ReferenceLibrary.ChannelType.BASE_Slave_Receive_0x00,
                channelID, (ushort) device.deviceNumber, device.deviceType, device.transType, (byte) device.radiofreq,
                (ushort) device.period, false);
            connected = true;
            deviceChannel.onReceiveData += Data;
            deviceChannel.onChannelResponse += ChannelResponse;

            deviceChannel.hideRXFAIL = true;
        }


        //Deal with the received Data
        private void Data(byte[] data)
        {
            sensorData.HeartRate = data[7];
        }


        private void ChannelResponse(ANT_Response response)
        {
        }
    }
}