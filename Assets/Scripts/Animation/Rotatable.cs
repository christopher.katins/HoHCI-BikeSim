﻿using System;
using UnityEngine;

namespace Animation
{
    public enum RotateAxis
    {
        Up,
        Right,
        Forward
    }

    [Serializable]
    public struct Rotatable
    {
        public GameObject gameObject;
        public Transform rotateAround;
        public RotateAxis rotateAxis;
    }
}