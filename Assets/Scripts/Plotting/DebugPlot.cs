﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Logging.Base;

namespace Plotting
{
    public class DebugPlot
    {
        private Process plottingProc;

        private readonly List<AsyncLogFileWriter> plotWriters = new List<AsyncLogFileWriter>();

        private DebugPlot()
        {
        }

        public AsyncLogFileWriter StartPlotting(string path)
        {
            var writer = new AsyncLogFileWriter(path);
            plotWriters.Add(writer);
            return writer;
        }

        public void ShowPlots()
        {
            plottingProc = Process.Start("CMD.exe", "/C cd Assets/Plotting & python plot.py");
        }

        private void Dispose()
        {
            plottingProc?.Kill();
            foreach (var p in plotWriters) p.Dispose();
        }

        #region signleton

        private static Lazy<DebugPlot>
            instance =
                new Lazy<DebugPlot>
                    (() => new DebugPlot());

        public static DebugPlot Instance => instance.Value;

        public static void DestroyInstance()
        {
            if (instance.IsValueCreated)
            {
                instance.Value.Dispose();
                instance =
                    new Lazy<DebugPlot>
                        (() => new DebugPlot());
            }
        }

        #endregion
    }
}