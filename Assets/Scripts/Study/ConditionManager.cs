﻿using System;
using Routes;
using Sensors;
using SicknessReduction.Haptic;
using SicknessReduction.Visual.DoF;
using SicknessReduction.Visual.Vignetting;
using UnityEngine;

namespace Study
{
    public enum SicknessMeasure
    {
        Vignetting,
        DepthOfField,
        VirtualHelmet,
        Fan,
        Vibration,
        None
    }

    [Serializable]
    public struct Condition
    {
        public int routeNumber;
        public SicknessMeasure activeMeasure;
    }

    public class ConditionManager : MonoBehaviour
    {
        [Header("Game Objects")] public RouteManager routeManager;
        public DynamicVignetting dynamicVignetting;
        public DynamicDoF dynamicDoF;
        public GameObject virtualHelmet;
        public DeskFanController fanController;
        public VibrationController vibrationController;
        public GameObject ppVolume;
        public SensorStatus waitForSensors;

        [Header("Configuration")] public Condition[] conditions;
        public int activeCondition;

        private void OnEnable()
        {
            var c = conditions[activeCondition];
            routeManager.selectedRoute = c.routeNumber;

            ppVolume.SetActive(false);

            fanController.enabled = c.activeMeasure == SicknessMeasure.Fan;
            vibrationController.enabled = c.activeMeasure == SicknessMeasure.Vibration;
            virtualHelmet.SetActive(c.activeMeasure == SicknessMeasure.VirtualHelmet);
            ppVolume.SetActive(c.activeMeasure == SicknessMeasure.DepthOfField);
            dynamicDoF.enabled = c.activeMeasure == SicknessMeasure.DepthOfField;
            dynamicVignetting.enabled = c.activeMeasure == SicknessMeasure.Vignetting;


            waitForSensors.waitForMqtt =
                c.activeMeasure == SicknessMeasure.Vibration || c.activeMeasure == SicknessMeasure.Fan;
        }
    }
}