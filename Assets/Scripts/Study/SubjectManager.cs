﻿using System;
using UnityEngine;

namespace Study
{
    public class SubjectManager : MonoBehaviour
    {
        public string subjectIdentifier;

        private void OnEnable()
        {
            if (string.IsNullOrEmpty(subjectIdentifier))
                throw new Exception("You have to specify a subject for the experiment!");
        }
    }
}