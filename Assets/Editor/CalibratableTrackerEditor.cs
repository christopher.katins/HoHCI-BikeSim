﻿using System;
using System.Globalization;
using Tracking;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public abstract class CalibratableTrackerEditor : UnityEditor.Editor
{
    protected CalibratableTracker tracker;

    protected virtual void OnEnable()
    {
        tracker = (CalibratableTracker) target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (!(EditorApplication.isPlaying || EditorApplication.isPaused)) return;

        tracker.bicycleTransform = (Transform) EditorGUILayout.ObjectField("Bicycle Transform", tracker.bicycleTransform,
            typeof(Transform), false);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Attach Vive Tracker to Wheel and press Button to calibrate");
        if (GUILayout.Button("Calibrate")) tracker.Calibrate();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Data", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Zero Rot:", tracker.ZeroRot.ToString());
        EditorGUILayout.LabelField("Zero Pos:", tracker.ZeroPos.ToString());

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Relative Position", tracker.RelativePosition.ToString());
        EditorGUILayout.LabelField("Relative Rotation", tracker.RelativeRotation.ToString());
       
    }
}