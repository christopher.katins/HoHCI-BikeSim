﻿using System;
using System.Globalization;
using Tracking;
using UnityEditor;
using UnityEditor.Recorder.Input;
using UnityEngine;

[CustomEditor(typeof(KineticLegTracker))]
// ReSharper disable once CheckNamespace
public class KineticLegTrackerEditor : CalibratableTrackerEditor
{
    private KineticLegTracker legTracker;

    protected override void OnEnable()
    {
        base.OnEnable();
        legTracker = (KineticLegTracker) tracker;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        /*if (!(EditorApplication.isPlaying || EditorApplication.isPaused)) return;
        EditorGUILayout.LabelField("Calculated Steer Rotation",
            fwt.SteerRotation.ToString(CultureInfo.CurrentCulture));*/
    }
}