﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(CustomWheelCollider))]
// ReSharper disable once CheckNamespace
public class CustomWheelColliderEditor : UnityEditor.Editor
{
    private CustomWheelCollider c;

    public void OnSceneGUI()
    {
        c = target as CustomWheelCollider;
        if (c == null) return;
        Handles.color = Color.magenta;
        var transform = c.transform;
        var position = transform.position;
        Handles.DrawWireDisc(position
            , transform.right // normal
            , c.radius); // radius
        var directions = c.CalculateRayDirections();
        foreach (var d in directions)
        {
            var direction = transform.TransformDirection(d);
            Handles.DrawLine(position, position + direction * c.radius);
        }
       
    }
}