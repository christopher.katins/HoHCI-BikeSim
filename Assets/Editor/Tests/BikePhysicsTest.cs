﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AdditionalMathf;
using NUnit.Framework;
using Phscs;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class BikePhysicsTest
{
    private const float DELTA = 1/3.6f;

    [Test]
    public void ZeroSlopeDoesntChangeSpeed()
    {
        const float speed = 30f/3.6f;
        var slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, 80f, 0f);
        Assert.AreEqual(speed, slopeSpeed, DELTA);
    }

    [Test]
    public void PositiveGradientAffectsSpeed()
    {
        const float speed = 30f/3.6f;
        const float mass = 80f;
        
        var grad = .05f;
        var slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(16.34544/3.6f, slopeSpeed, DELTA);

        grad = .1f;
        slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(9.44704/3.6f, slopeSpeed, DELTA);
        
        grad = .15f;
        slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(6.43698/3.6f, slopeSpeed, DELTA);
    }
    
    [Test]
    public void NegativeGradientAffectsSpeed()
    {
        const float speed = 30f/3.6f;
        const float mass = 80f;
        
        var grad = -.05f;
        var slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(44.5824/3.6f, slopeSpeed, DELTA);

        grad = -.1f;
        slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(56.91384/3.6f, slopeSpeed, DELTA);
        
        grad = -.15f;
        slopeSpeed = BicyclePhysics.SpeedAtGradientForSpeedAtFlat(speed, mass, grad);
        Assert.AreEqual(67.43808/3.6f, slopeSpeed, DELTA);
    }
}