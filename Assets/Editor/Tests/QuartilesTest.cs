﻿using System;
using System.Collections;
using System.Collections.Generic;
using AdditionalMathf;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class QuartilesTest
{
    private const float DELTA = 0.05f;

    [Test]
    public void Length1()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var quartiles = new Quartiles(new List<float>());
        });
    }

    [Test]
    public void Length2()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var quartiles = new Quartiles(new List<float>());
        });
    }

    [Test]
    public void Length3()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var quartiles = new Quartiles(new List<float>());
        });
    }

    [Test]
    public void Length4()
    {
        var q = new Quartiles(new[] {1.5f, 3.5f, 7f, 12.3f});
        Assert.AreEqual(1.5f, q.Q1, DELTA);
        Assert.AreEqual(3.5f, q.Q2, DELTA);
        Assert.AreEqual(7f, q.Q3, DELTA);
    }

    [Test]
    public void Empty()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var quartiles = new Quartiles(new List<float>());
        });
    }

    [Test]
    public void IQR()
    {
        var q = new Quartiles(new[]
        {
            1.5f, 3.5f,
            7f, 12.3f,
            15f, 17.25f,
            18f, 22.27f
        }); //8
        Assert.AreEqual(q.Iqr, q.Q3 - q.Q1, DELTA);
    }

    [Test]
    public void Even()
    {
        var q1 = new Quartiles(new[]
        {
            1.5f, 3.5f,
            7f, 12.3f,
            15f, 17.25f,
            18f, 22.27f
        }); //8
        Assert.AreEqual(3.5f, q1.Q1, DELTA);
        Assert.AreEqual(12.3f, q1.Q2, DELTA);
        Assert.AreEqual(17.25f, q1.Q3, DELTA);
        var q2 = new Quartiles(new[]
        {
            1.5f, 3.5f, 7f,
            12.3f, 15f, 17.25f,
            18f, 22.27f, 25f,
            28.5f, 33f, 39.5f
        }); //12
        Assert.AreEqual(7f, q2.Q1, DELTA);
        Assert.AreEqual(17.25f, q2.Q2, DELTA);
        Assert.AreEqual(25f, q2.Q3, DELTA);
    }

    [Test]
    public void Uneven()
    {
        var q1 = new Quartiles(new[] {
            1.5f, 3.5f, 
            7f, 12.3f, 
            15f, 
            17.25f, 18f, 
            22.27f, 25f}); //9 [2.25, 4.5, 6.75]
        Assert.AreEqual(Mathf.Lerp(3.5f, 7f, .25f), q1.Q1, DELTA);
        Assert.AreEqual(Mathf.Lerp(12.3f, 15f, .5f), q1.Q2, DELTA);
        Assert.AreEqual(Mathf.Lerp(17.25f, 18f, .75f), q1.Q3, DELTA);
        var q2 = new Quartiles(new[]
            {1.5f, 3.5f, 7f, 12.3f, 15f, 17.25f, 18f, 22.27f, 25f, 28.5f, 33f, 39.5f, 45f}); //13 [3.25, 6.5, 9.75]
        Assert.AreEqual(Mathf.Lerp(7f, 12.3f, .25f), q2.Q1, DELTA);
        Assert.AreEqual(Mathf.Lerp(17.25f, 18f, .5f), q2.Q2, DELTA);
        Assert.AreEqual(Mathf.Lerp(25f, 28.5f, .75f), q2.Q3, DELTA);
    }
}