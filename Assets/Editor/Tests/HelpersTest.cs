﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AdditionalMathf;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class HelpersTest
{
    private const float DELTA = 0.05f;

    [Test]
    public void RemoveOutliersFound()
    {
        var items = new float[]
        {
            1f, 2f, 1.5f, 0.75f, 0.66f, 1.2f, 1.3f, 25f, -10f, 33f
        };
        var cleaned = Helpers.RemoveOutliers(items).ToList();
        Assert.AreEqual(items.Length-3, cleaned.Count);
        Assert.False(cleaned.Contains(25f));
        Assert.False(cleaned.Contains(-10f));
        Assert.False(cleaned.Contains(33f));
    }

    [Test]
    public void RemoveOutliersNothingFound()
    {
        var items = new float[]
        {
            1f, 2f, 1.5f, 0.75f, 0.66f, 1.2f, 1.3f
        };
        var cleaned = Helpers.RemoveOutliers(items).ToList();
        Assert.AreEqual(items, cleaned);
    }
}