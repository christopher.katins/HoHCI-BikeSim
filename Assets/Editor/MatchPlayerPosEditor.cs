﻿using Tracking;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(MatchPlayerPositionAndCalibrate))]
public class MatchPlayerPosEditor : UnityEditor.Editor
{
    private MatchPlayerPositionAndCalibrate matcher;
    protected virtual void OnEnable()
    {
        matcher = (MatchPlayerPositionAndCalibrate) target;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (!(EditorApplication.isPlaying || EditorApplication.isPaused)) return;
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Make sure that trackers are available, and set the front wheel straight.");
        if (GUILayout.Button("Match")) matcher.Match();
       // if (GUILayout.Button("MatchLg")) matcher.MatchLeg();
    }
}