﻿using System;
using System.Globalization;
using Tracking;
using UnityEditor;
using UnityEditor.Recorder.Input;
using UnityEngine;

[CustomEditor(typeof(FrontWheelTracker))]
// ReSharper disable once CheckNamespace
public class FrontWheelTrackerEditor : CalibratableTrackerEditor
{
    private FrontWheelTracker fwt;

    protected override void OnEnable()
    {
        base.OnEnable();
        fwt = (FrontWheelTracker) tracker;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (!(EditorApplication.isPlaying || EditorApplication.isPaused)) return;
        EditorGUILayout.LabelField("Calculated Steer Rotation",
            fwt.SteerRotation.ToString(CultureInfo.CurrentCulture));
    }
}